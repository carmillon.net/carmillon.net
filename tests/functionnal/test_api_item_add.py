#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tests.conftest import api_url


def test_api_item_add_description(test_client, login_admin_user, login_bot_user, login_modo_user):
    bot_token = login_bot_user.get_json()['response']['data']['auth_token']
    # admin_token = login_admin_user.get_json()['response']['data']['auth_token']
    modo_token = login_modo_user.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/items',
                          json={'description': '',
                                'adresse': '1 rue du test',
                                'categorie': 'Salle de torture',
                                'enseigne': 'AU DONJON NATIONAL'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )

    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Le champ description doit être renseigné.'

    rv = test_client.post(api_url + '/items',
                          json={'description': '50 % de réduction',
                                'adresse': '',
                                'categorie': 'Salle de torture',
                                'enseigne': 'AU DONJON NATIONAL'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )

    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C6002'
    assert rv.get_json()['response']['text'] == 'Le champ adresse doit être renseigné.'

    rv = test_client.post(api_url + '/items',
                          json={'description': '50 % de réduction',
                                'adresse': '1 rue du test',
                                'categorie': '',
                                'enseigne': 'AU DONJON NATIONAL'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )

    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C6003'
    assert rv.get_json()['response']['text'] == 'Le champ categorie doit être renseigné.'

    rv = test_client.post(api_url + '/items',
                          json={'description': '50 % de réduction',
                                'adresse': '1 rue du test',
                                'categorie': 'Salle de torture',
                                'enseigne': ''},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )

    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C6004'
    assert rv.get_json()['response']['text'] == 'Le champ enseigne doit être renseigné.'

    rv = test_client.post(api_url + '/items',
                          json={'description': '50 % de réduction',
                                'adresse': '1 rue du test',
                                'categorie': 'Salle de torture',
                                'enseigne': 'AU DONJON NATIONAL'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )

    assert rv.status_code == 201
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6005'
    assert rv.get_json()['response']['text'] == 'L\'avantage a été créé, en attente de validation par un modérateur.'

    rv = test_client.get(api_url + '/items',
                         json={'enseigne': 'AU DONJON NATIONAL'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['data'][0]['status_code'] == 'PROPOSE-01'

    rv = test_client.post(api_url + '/items',
                          json={'description': '50 % de réduction',
                                'adresse': '2 rue du test',
                                'categorie': 'Sorcellerie',
                                'enseigne': 'AU PARCHEMIN MAGIQUE'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': modo_token}
                          )

    assert rv.status_code == 201
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6006'
    assert rv.get_json()['response']['text'] == 'L\'avantage a été créé.'

    rv = test_client.get(api_url + '/items',
                         json={'recherche': 'Sorcellerie'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['data'][0]['status_code'] == 'CONFIRME-01'

    rv = test_client.get(api_url + '/items',
                         json={'recherche': 'AU DONJON NATIONAL'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': modo_token}
                         )
    item_id = rv.get_json()['response']['data'][0]['id']

    rv = test_client.put(api_url + '/items',
                         json={'id': item_id,
                               'statut': 'CONFIRME-01'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 401

    rv = test_client.get(api_url + '/items',
                         json={'id': item_id},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.get_json()['response']['data'][0]['status_code'] == 'PROPOSE-01'

    rv = test_client.put(api_url + '/items',
                         json={'id': item_id,
                               'statut': 'CONFIRME-01'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': modo_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True

    rv = test_client.get(api_url + '/items',
                         json={'id': item_id},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )

    assert rv.get_json()['response']['data'][0]['status_code'] == 'CONFIRME-01'

    rv = test_client.get(api_url + '/items',
                         json={'recherche': 'AU PARCHEMIN MAGIQUE'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': modo_token}
                         )
    item_id = rv.get_json()['response']['data'][0]['id']

    rv = test_client.put(api_url + '/items',
                         json={'id': item_id,
                               'statut': 'PROPOSE-01'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': modo_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True

    rv = test_client.get(api_url + '/items',
                         json={'recherche': 'AU PARCHEMIN MAGIQUE'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': modo_token}
                         )

    assert rv.get_json()['response']['data'][0]['status_code'] == 'PROPOSE-01'

    rv = test_client.get(api_url + '/items',
                         json={'id': item_id},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data'] == []

    rv = test_client.get(api_url + '/items',
                         json={'recherche': 'AU PARCHEMIN MAGIQUE'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200
    print(rv.get_json())
    assert rv.get_json()['response']['data'] == []
