#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from tests.conftest import api_url
from datetime import datetime


def test_api_comment_post(test_client, login_bot_user, login_admin_user):
    auth_token = login_bot_user.get_json()['response']['data']['auth_token']

    rv = login_admin_user
    auth_token_admin = rv.get_json()['response']['data']['auth_token']

    file = open('../carmillon.csv', 'rb')
    files = {'files': file}

    rv = test_client.post(api_url + '/items/import',
                          data=files,
                          headers={'content-type': 'multipart/form-data',
                                   'Authentication-Token': auth_token_admin}
                          )

    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire qui valide',
                                'validation': True},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Commentaire posté avec succes'

    rv = test_client.post(api_url + '/items/2/commentaires',
                          json={'message': 'Ceci est un commentaire'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Commentaire posté avec succes'

    rv = test_client.post(api_url + '/items/3/commentaires',
                          json={'message': 'Ceci est un commentaire',
                                'date': "2019-01-01"},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Commentaire posté avec succes'

    rv = test_client.post(api_url + '/items/4/commentaires',
                          json={'validation': True,},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Commentaire posté avec succes'

    rv = test_client.post(api_url + '/items/5/commentaires',
                          json={'validation': True,
                                'date': "2019-01-01"},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6001'
    assert rv.get_json()['response']['text'] == 'Commentaire posté avec succes'

    rv = test_client.post(api_url + '/items/6/commentaires',
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )

    assert rv.status_code == 400

    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire'}
                          )

    assert rv.status_code == 401


def test_api_commentaires_get(test_client, login_bot_user, login_admin_user):
    auth_token = login_bot_user.get_json()['response']['data']['auth_token']
    rv = login_admin_user
    auth_token_admin = rv.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire qui valide',
                                'validation': True,
                                'date': '2019-01-01'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token_admin}
                          )

    rv = test_client.get(api_url + '/items/1/commentaires',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6101'
    assert rv.get_json()['response']['text'] == 'Commentaires recuperes avec succes'
    assert rv.get_json()['response']['data']['commentaires'][1]['message'] == 'Ceci est un commentaire qui valide'
    assert rv.get_json()['response']['data']['commentaires'][1]['validation'] is True
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Ceci est un commentaire'
    assert rv.get_json()['response']['data']['commentaires'][0]['validation'] is False
    assert rv.get_json()['response']['data']['nombre'] == 2
    assert rv.get_json()['response']['data']['validations'] == 1
    assert rv.get_json()['response']['data']['non_validations'] == 1
    assert rv.get_json()['response']['data']['derniere_validation'] == datetime(2019, 1, 1).isoformat()

    rv = test_client.get(api_url + '/items/2/commentaires',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6101'
    assert rv.get_json()['response']['text'] == 'Commentaires recuperes avec succes'
    assert rv.get_json()['response']['data']['nombre'] == 0
    assert rv.get_json()['response']['data']['validations'] == 0
    assert rv.get_json()['response']['data']['non_validations'] == 0
    assert rv.get_json()['response']['data']['derniere_validation'] is None

    rv = test_client.get(api_url + '/items/1/commentaires/2',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6102'
    assert rv.get_json()['response']['text'] == 'Commentaire recupere avec succes'
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Ceci est un commentaire'
    assert rv.get_json()['response']['data']['commentaires'][0]['validation'] is False

    rv = test_client.get(api_url + '/items/1/commentaires/2',
                         headers={'content-type': 'application/json'}
                         )
    assert rv.status_code == 401


def test_api_commentaires_put(test_client, login_bot_user, login_admin_user):
    admin_token = login_admin_user.get_json()['response']['data']['auth_token']
    auth_token = login_bot_user.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire qui valide',
                                'validation': True},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )

    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'message': 'Ceci est un commentaire qui ne valide plus',
                               'validation': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C6201'
    assert rv.get_json()['response']['text'] == 'Commentaire modifié avec succès'

    rv = test_client.get(api_url + '/items/1/commentaires/1',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Ceci est un commentaire qui ne valide plus'
    assert rv.get_json()['response']['data']['commentaires'][0]['validation'] is False

    rv = test_client.post(api_url + '/items/1/commentaires',
                          json={'message': 'Ceci est un commentaire d admin',
                                'validation': True},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': admin_token}
                          )

    rv = test_client.get(api_url + '/items/1/commentaires/2',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Ceci est un commentaire d admin'

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'message': 'Modification du message d admin',
                               'validation': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 403

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'message': 'Modification du message d admin par l admin',
                               'validation': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Modification du message d admin par l admin'
    assert rv.get_json()['response']['data']['commentaires'][0]['validation'] is False
    assert rv.get_json()['response']['data']['commentaires'][0]['modifie_par'] == 'admin'
    assert rv.get_json()['response']['data']['commentaires'][0]['modifie_le'][0:13] == datetime.utcnow()\
                                                                                               .isoformat("T", "hours")
    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'message': 'Modification du message par l admin',
                               'validation': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['commentaires'][0]['message'] == 'Modification du message par l admin'
    assert rv.get_json()['response']['data']['commentaires'][0]['validation'] is False

    rv = test_client.get(api_url + '/items/1/commentaires',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['nombre'] == 2
    assert rv.get_json()['response']['data']['validations'] == 0
    assert rv.get_json()['response']['data']['non_validations'] == 2

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'validation': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200

    rv = test_client.get(api_url + '/items/1/commentaires',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['nombre'] == 2
    assert rv.get_json()['response']['data']['validations'] == 1
    assert rv.get_json()['response']['data']['non_validations'] == 1

    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'activer': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200

    rv = test_client.get(api_url + '/items/1/commentaires',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )

    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['nombre'] == 1

    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'activer': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 403

    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'activer': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200

    rv = test_client.put(api_url + '/items/1/commentaires/1',
                         json={'activer': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200

    assert rv.get_json()['response']['data']['nombre'] == 2

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'alerte': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte'] is True
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_le'][0:13] == datetime.utcnow()\
                                                                                              .isoformat("T", "hours")
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_par'] == 'bot'

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'alerte': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte'] is False
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_le'] is None
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_par'] is None

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'alerte': True},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': admin_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte'] is True
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_le'][0:13] == datetime.utcnow()\
                                                                                              .isoformat("T", "hours")
    assert rv.get_json()['response']['data']['commentaires'][0]['alerte_par'] == 'admin'

    rv = test_client.put(api_url + '/items/1/commentaires/2',
                         json={'alerte': False},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 403
