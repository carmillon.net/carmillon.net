#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tests.conftest import api_url
from flask import current_app
import jwt


def test_api_auth_register(test_client, login_admin_user):

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'bot@bot.com',
                                'password': 'bot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0103'
    assert rv.get_json()['response']['text'] == 'Erreur, seules les adresses sncf.fr sont acceptées'

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'ext.mr.bot@sncf.fr',
                                'password': 'bot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0103'
    assert rv.get_json()['response']['text'] == 'Erreur, les addresses mail externes ne sont pas acceptées'

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'mr.bot@sncf.fr',
                                'password': ''},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0103'
    assert rv.get_json()['response']['text'] == 'Erreur, le mot de passe est vide'

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'mr@bot@sncf.fr',
                                'password': 'bot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0103'
    assert rv.get_json()['response']['text'] == 'Erreur, le mail a plusieurs @'

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': '',
                                'password': 'bot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0103'
    assert rv.get_json()['response']['text'] == 'Erreur, le mail est vide'

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'mr.bot@sncf.fr',
                                'password': 'bot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C0101'
    assert rv.get_json()['response']['text'] == 'Enregistrement: OK. Un mail a été envoyé pour confirmer le compte.'


def test_api_auth_login(test_client, login_bot_user):
    rv = login_bot_user
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C0201'
    assert rv.get_json()['response']['text'] == 'Connexion réussie'
    token = rv.get_json()['response']['data']['auth_token']
    payload = jwt.decode(token,
                         current_app.config.get('SECRET_KEY'),
                         algorithms=['HS256'])
    assert payload['sub'] == 2
    rv = test_client.post(api_url + '/auth/login',
                          json={'email': 'bot@lavergne.online',
                                'password': 'notbot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 401
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0203'
    assert rv.get_json()['response']['text'] == 'Le mot de passe donné est incorrect'
    assert rv.get_json()['response']['data']['auth_token'] is None
    rv = test_client.post(api_url + '/auth/login',
                          json={'email': 'bot1@sncf.fr',
                                'password': 'notbot1'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 404
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0202'
    assert rv.get_json()['response']['text'] == 'L\'email "bot1@sncf.fr" n\'existe pas'
    assert rv.get_json()['response']['data']['auth_token'] is None


def test_api_auth_logout(test_client, login_bot_user):
    rv = login_bot_user
    auth_token = rv.get_json()['response']['data']['auth_token']
    rv = test_client.post(api_url + '/auth/logout',
                          json={'email': 'bot@lavergne.online'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C0301'
    assert rv.get_json()['response']['text'] == 'Logout OK'
    rv = test_client.post(api_url + '/auth/logout',
                          json={'email': 'bot1@sncf.fr'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 404
    assert rv.get_json()['response']['success'] is False
    assert rv.get_json()['response']['code_app'] == 'C0302'
    assert rv.get_json()['response']['text'] == 'L\'email "bot1@sncf.fr" n\'existe pas'


def test_api_auth_list(test_client, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']
    rv = test_client.get(api_url + '/auth/users',
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': auth_token}
                         )
    assert rv.status_code == 200


def test_api_auth_group(test_client, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/auth/register',
                          json={'email': 'bot2@sncf.fr',
                                'password': 'bot2'},
                          headers={'content-type': 'application/json'}
                          )
    assert rv.status_code == 200

    rv = test_client.post(api_url + '/auth/change_role',
                          json={'email': 'bot2@sncf.fr', 'role': 'modo'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 201
    assert rv.get_json()['response']['success'] is True
    assert rv.get_json()['response']['code_app'] == 'C0501'
    assert rv.get_json()['response']['text'] == 'Ajout du rôle modo OK'


def test_api_auth_lower(test_client, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/auth/register_restricted',
                          json={'email': 'bot3@lavergne.online',
                                'password': 'bot3'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200

    rv = test_client.post(api_url + '/auth/login',
                          json={'email': 'Bot3@laVergne.Online',
                                'password': 'bot3'},
                          headers={'content-type': 'application/json'}
                          )

    assert rv.status_code == 200

    rv = test_client.post(api_url + '/auth/register_restricted',
                          json={'email': 'bOt4@Lavergne.OnlinE',
                                'password': 'bot4'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200

    rv = test_client.post(api_url + '/auth/login',
                          json={'email': 'bot4@lavergne.online',
                                'password': 'bot4'},
                          headers={'content-type': 'application/json'}
                          )

    assert rv.status_code == 200
