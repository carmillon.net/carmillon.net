#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tests.conftest import api_url


def test_api_brand(test_client, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/auth/register_restricted',
                          json={'email': 'bot2@bot2.com',
                                'password': 'bot2'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 200

    rv = test_client.post(api_url + '/auth/change_role',
                          json={'email': 'bot2@bot2.com', 'role': 'modo'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': auth_token}
                          )
    assert rv.status_code == 201

    rv = test_client.post(api_url + '/auth/login',
                          json={'email': 'bot2@bot2.com',
                                'password': 'bot2'},
                          headers={'content-type': 'application/json'}
                          )

    bot_token = rv.get_json()['response']['data']['auth_token']

    rv = test_client.post(api_url + '/enseignes',
                          json={'code': 'APPLE-01', 'nom': 'Apple'},
                          headers={'content-type': 'application/json',
                                   'Authentication-Token': bot_token}
                          )
    assert rv.status_code == 201
    assert rv.get_json()['response']['text'] == 'Ajout OK'

    rv = test_client.get(api_url + '/enseignes',
                         json={'code': 'APPLE-01'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data'][0]['nom'] == 'Apple'

    rv = test_client.get(api_url + '/enseignes',
                         json={'nom': 'Apple'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200

    brand_id = rv.get_json()['response']['data'][0]['id']

    rv = test_client.put(api_url + '/enseignes',
                         json={'id': brand_id, 'nom': 'Apple_New'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200

    rv = test_client.get(api_url + '/enseignes',
                         json={'nom': 'Apple_New'},
                         headers={'content-type': 'application/json',
                                  'Authentication-Token': bot_token}
                         )
    assert rv.status_code == 200
    assert rv.get_json()['response']['data'][0]['code'] == 'APPLE-01'
