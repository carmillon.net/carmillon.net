#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from tests.conftest import api_url


def test_api_import_items(test_client, login_bot_user, login_admin_user):
    rv = login_bot_user
    # auth_token_bot = rv.get_json()['response']['data']['auth_token']
    # bot_headers = {'content-type': 'application/json', 'Authentication-Token': auth_token_bot}

    rv = login_admin_user
    auth_token_admin = rv.get_json()['response']['data']['auth_token']

    file = open('../carmillon.csv', 'rb')
    files = {'files': file}

#    data = {'file': (file, 'carmillon.csv'),}

    rv = test_client.post(api_url + '/items/import',
                          data=files,
                          headers={'content-type': 'multipart/form-data',
                                   'Authentication-Token': auth_token_admin}
                          )

    rv = test_client.get(api_url + '/items',
                         headers={"accept": "text/csv"})
