#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from carmillon.app import create_app
from carmillon.extensions import db

api_url = '/api/v0'


@pytest.fixture
def test_client():
    flask_app = create_app('testing')

    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = flask_app.test_client()

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()


@pytest.fixture
def create_database():
    # Create the database and the database table
    db.create_all()

    yield db  # this is where the testing happens!

    db.drop_all()


@pytest.fixture
def init_database(create_database, test_client):
    test_client.post(api_url + '/db/init')
    return test_client


@pytest.fixture
def login_admin_user(init_database):
    rv = init_database.post(api_url + '/auth/login',
                            json={'email': 'admin@admin.com',
                                  'password': 'admin'},
                            headers={'content-type': 'application/json'}
                            )
    return rv


@pytest.fixture
def create_bot_user(init_database, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']
    rv = init_database.post(api_url + '/auth/register_restricted',
                            json={'email': 'bot@lavergne.online',
                                  'password': 'bot1'},
                            headers={'content-type': 'application/json',
                                     'Authentication-Token': auth_token}
                            )

    return rv


@pytest.fixture
def login_bot_user(init_database, create_bot_user):
    rv = init_database.post(api_url + '/auth/login',
                            json={'email': 'bot@lavergne.online',
                                  'password': 'bot1'},
                            headers={'content-type': 'application/json'}
                            )
    return rv


@pytest.fixture
def create_modo_user(init_database, login_admin_user):
    auth_token = login_admin_user.get_json()['response']['data']['auth_token']
    init_database.post(api_url + '/auth/register_restricted',
                       json={'email': 'modo@lavergne.online',
                             'password': 'modo'},
                       headers={'content-type': 'application/json',
                                'Authentication-Token': auth_token}
                       )

    rv = init_database.post(api_url + '/auth/change_role',
                            json={'email': 'modo@lavergne.online', 'role': 'modo'},
                            headers={'content-type': 'application/json',
                                     'Authentication-Token': auth_token}
                            )

    return rv


@pytest.fixture
def login_modo_user(init_database, create_modo_user):
    rv = init_database.post(api_url + '/auth/login',
                            json={'email': 'modo@lavergne.online',
                                  'password': 'modo'},
                            headers={'content-type': 'application/json'}
                            )
    return rv
