/* Global Variables */
var timerId;

/* Utilities */
function showModal(){
  /* TODO Migrate from JQuery */
  $("#Modal").modal();
}
function hideModal(){
  /* TODO Migrate from JQuery */
  $('#Modal').modal('hide');
}
function getLog(title, message){
  if (getDebug()){
      console.log(title + ' : ' + message);
  }
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function deleteCookie(cname) {
    var expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC";
    document.cookie = cname + "=" + ";" + expires + ";path=/";
}
function obtenirParametre (sVar) {
  return unescape(window.location.search.replace(
  new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

/* Initial SetUp */
function setUpIndex(){
  fetchNavBar();
  fetchFooter();
  fetchModal();
  fetchContent();
  /* TODO Migrate from JQuery */
  $(window).on('popstate', function(){loadHistory(event)});
}
function fetchNavBar(){
  fetch("pages/nav-bar.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data) {
    document.getElementById("nav-placeholder").innerHTML = data;
    var username = getCookie("user_username");
    if(username != ""){
      document.getElementById("navLogin").textContent = username;
    }
  })
}
function fetchFooter(){
  fetch("pages/footer.html")
  .then(function(response){
    return response.text()
  })
  .then(function(data){
    document.getElementById("footer-placeholder").innerHTML = data
  })
}
function fetchModal(){
  fetch("pages/modal.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("modal-placeholder").innerHTML = data
  })
}
function fetchContent(){
  fetch("pages/dept.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("deptContainer").innerHTML = data
  })
  fetch("pages/brand.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("brandContainer").innerHTML = data
  })
  fetch("pages/category.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("categoryContainer").innerHTML = data
  })
  fetch("pages/location.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("locationContainer").innerHTML = data
  })
  fetch("pages/status.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("statusContainer").innerHTML = data
  })
  fetch("pages/ajoutItem.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("itemAjoutContainer").innerHTML = data
  })
  fetch("pages/ajoutBrand.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("brandAjoutContainer").innerHTML = data
  })
  fetch("pages/item.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("itemContainer").innerHTML = data
  })
  .then(function(data){
    testItem();
  })
  fetch("pages/login.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("loginContainer").innerHTML = data
  })
  fetch("pages/profil.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("profilContainer").innerHTML = data
  })
  fetch("pages/confirm.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("invalidContainer").innerHTML = data
  })
  .then(function(data){
    testConfirm();
  })
  fetch("pages/connectionNeeded.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("connectionNeededContainer").innerHTML = data
  })
  fetch("pages/searchItems.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
   document.getElementById("searchContainer").innerHTML = data
  })

  var username = getCookie("user_username");
  if(username == ""){
      document.getElementById("connectionNeededContainer").classList.add('containerShow');
  }
  else{
      document.getElementById("searchContainer").classList.add('containerShow');
      checkRole("modo", indexAddModoSetUp);
  }

}
function cleanContainers(){
  document.getElementById("deptContainer").classList.remove('containerShow');
  document.getElementById("brandContainer").classList.remove('containerShow');
  document.getElementById("categoryContainer").classList.remove('containerShow');
  document.getElementById("locationContainer").classList.remove('containerShow');
  document.getElementById("statusContainer").classList.remove('containerShow');
  document.getElementById("itemContainer").classList.remove('containerShow');
  document.getElementById("itemAjoutContainer").classList.remove('containerShow');
  document.getElementById("brandAjoutContainer").classList.remove('containerShow');
  document.getElementById("loginContainer").classList.remove('containerShow');
  document.getElementById("profilContainer").classList.remove('containerShow');
  document.getElementById("invalidContainer").classList.remove('containerShow');
  document.getElementById("connectionNeededContainer").classList.remove('containerShow');
  document.getElementById("searchContainer").classList.remove('containerShow');
  document.getElementById("connectionNeededContainer").classList.remove('containerShow');

  document.getElementById("commentItemUserID").value = "";
  document.getElementById("commentTextItem").innerHTML = "";
  document.getElementById("commentTextItem").text = "";
  document.getElementById("commentTextItem").value = "";
}
function testConfirm(){
    var confirm_token = obtenirParametre("confirm_token");
    if (confirm_token != "") {
      cleanContainers();
      document.getElementById("invalidContainer").classList.add('containerShow');
      confirmToken(confirm_token);
    } else {
      getLog("testConfirm", "Invalid");
    }
}

function testItem(){
    var item = obtenirParametre("item");
    if (item != "") {
      cleanContainers();
      document.getElementById("itemContainer").classList.add('containerShow');
      loadItemPage(item);
    } else {
      getLog("testConfirm", "Invalid");
    }
}

/* Pages */
function loginCheck(){
  var username = getCookie("user_username");
  if(username == ""){
    document.getElementById("connectionNeededContainer").classList.add('containerShow');
    return false;
  }
  else {
    var username = getCookie("user_username");
    if(username != ""){
      if (document.getElementById("navLogin")){
        document.getElementById("navLogin").textContent = username;
      }
    }
    checkRole("admin", loginAdminSetUp);
    checkRole("modo", loginModoSetUp);
    return true;
  }
}
function getIndex(){
  getLog("goIndex", "In");
  cleanContainers();
  var loginOK = loginCheck();
  if (loginOK == true) {
      document.getElementById("searchContainer").classList.add('containerShow');
      getLog("getIndex loginOK", "In");
      checkRole("modo", indexAddModoSetUp);
  }
}

function getIndexBack(){
  getLog("getIndexBack", "In");
  history.back();
  getIndex();
}

function getLogin(){
  getLog("goLogin", "In");
  window.history.pushState({page: 'login'}, 'Détail', '');
  cleanContainers();
  var loginOK = loginCheck();
  if (loginOK == true) {
    document.getElementById("profilContainer").classList.add('containerShow');
  }
  else{
    document.getElementById("connectionNeededContainer").classList.remove('containerShow');
    document.getElementById("loginContainer").classList.add('containerShow');
  }
}
function getLogout(){
  logoutSetUp();
  document.getElementById("navLogin").textContent = "Connexion";
  deleteCookie("user_id");
  deleteCookie("user_username");
  deleteCookie("user_token");
  deleteCookie("user_email");
  deleteCookie("user_token_date");
  getIndex();
}
function loginAdminSetUp(){
  document.getElementById(
  "changeRoleForm").classList.remove('d-none');
}
function indexAddModoSetUp(){
  getLog("indexAddModoSetUp", "In");
  itemModoSetUp();
}
function itemModoSetUp(){
  document.getElementById("adresseTextItem").readOnly = false;
  document.getElementById("avantageTextItem").readOnly = false;
  if (document.getElementById("emplacementsNomItem").innerHTML == ""){
    document.getElementById("emplacementsNomItem").disabled = false;
    getList('emplacements');
  }
  document.getElementById("statutsNomItem").disabled = false;
  document.getElementById("categoriesNomItem").disabled = false;
  document.getElementById("enseignesNomItem").disabled = false;
  document.getElementById("saveButtonItem").classList.remove('d-none');
  getList('enseignes');
  getList('categories');
  getList('statuts');
}

function loginModoSetUp(){

  document.getElementById("exportItemsCsv").classList.remove('d-none');

  itemModoSetUp();

  document.getElementById("departementsNom").readOnly = false;
  document.getElementById("departementsCode").readOnly = false;
  document.getElementById("saveButtonDept").classList.remove('d-none');

  document.getElementById("enseignesNom").readOnly = false;
  document.getElementById("enseignesCode").readOnly = false;
  document.getElementById("enseignesEditItem").classList.remove('d-none');
  document.getElementById("saveButtonBrand").classList.remove('d-none');

  document.getElementById("categoriesNom").readOnly = false;
  document.getElementById("categoriesCode").readOnly = false;
  document.getElementById("saveButtonCategory").classList.remove('d-none');

  document.getElementById("emplacementsNom").readOnly = false;
  document.getElementById("emplacementsCode").readOnly = false;
  document.getElementById("emplacementsEditItem").classList.remove('d-none');
  document.getElementById("saveButtonLocation").classList.remove('d-none');

  document.getElementById("statutsNom").readOnly = false;
  document.getElementById("statutsCode").readOnly = false;
  document.getElementById("saveButtonStatus").classList.remove('d-none');
}
function logoutSetUp(){
  document.getElementById("exportItemsCsv").classList.add('d-none');
  document.getElementById("changeRoleForm").classList.add('d-none');

  document.getElementById("adresseTextItem").readOnly = true;
  document.getElementById("avantageTextItem").readOnly = true;
  document.getElementById("departementsNomItem").disabled = true;
  document.getElementById("emplacementsNomItem").disabled = true;
  document.getElementById("statutsNomItem").disabled = true;
  document.getElementById("categoriesNomItem").disabled = true;
  document.getElementById("enseignesNomItem").disabled = true;
  document.getElementById("saveButtonItem").classList.add('d-none');

  if (document.getElementById("departementsNomItemAjout")){
    document.getElementById("departementsNomItemAjout").disabled = true;
  }
  document.getElementById("emplacementsNomItemAjout").disabled = true;
  document.getElementById("statutsNomItemAjout").disabled = true;
  document.getElementById("categoriesNomItemAjout").disabled = true;
  document.getElementById("enseignesNomItemAjout").disabled = true;
  document.getElementById("saveButtonItemAjout").classList.add('d-none');

  document.getElementById("departementsNom").readOnly = true;
  document.getElementById("departementsCode").readOnly = true;
  document.getElementById("saveButtonDept").classList.add('d-none');

  document.getElementById("enseignesNom").readOnly = true;
  document.getElementById("enseignesCode").readOnly = true;
  document.getElementById("enseignesEditItem").classList.add('d-none');
  document.getElementById("saveButtonBrand").classList.add('d-none');

  document.getElementById("categoriesNom").readOnly = true;
  document.getElementById("categoriesCode").readOnly = true;
  document.getElementById("saveButtonCategory").classList.add('d-none');

  document.getElementById("emplacementsNom").readOnly = true;
  document.getElementById("emplacementsCode").readOnly = true;
  document.getElementById("emplacementsEditItem").classList.add('d-none');
  document.getElementById("saveButtonLocation").classList.add('d-none');

  document.getElementById("statutsNom").readOnly = true;
  document.getElementById("statutsCode").readOnly = true;
  document.getElementById("saveButtonStatus").classList.add('d-none');
}

function loadHistory(event){
    getLog("loadHistory","location: " + document.location + ", state: " + JSON.stringify(event.state));
    getIndex();
}

function loadItemPage(id){
  loadPage(
  "item", id);
  window.history.pushState({page: 'item'}, 'Détail', '/index.html?item='+id);
}
function loadDeptPage(id){
  loadPage(
  "dept", id);
}
function loadBrandPage(id){
  loadPage(
  "brand", id);
}
function loadCategoryPage(id){
  loadPage(
  "category", id);
}
function loadLocationPage(id){
  loadPage(
  "location", id);
}
function loadStatusPage(id){
  loadPage(
  "status", id);
}
function loadPage(page, id){
  cleanContainers();
  getLog("loadPage page", page);
  getLog("loadPage id", id);
  var loginOK = loginCheck();
  if (loginOK == true) {
    var array = {
      'dept': 'departements',
      'brand': 'enseignes',
      'category': 'categories',
      'location': 'emplacements',
      'status': 'statuts'
    }
    if (page=='item'){
      getItem(id);
    }
    else
    {
      if (id){
        getFromID(id ,array[page])
      }
    }

    container = page + "Container";
    document.getElementById(container).classList.add('containerShow');
    document.getElementById("searchContainer").classList.remove('containerShow');
  }
  else{
    document.getElementById("searchContainer").classList.remove('containerShow');
  }
}

/* CallBack */
function loginButtonClick(){
  setUpModal("Connexion", "Chargement en cours", "")
  loginAPI();
}
function registerButtonClick(){
  if (document.getElementById('conditionsCheck').checked == true){
    setUpModal("Enregistrement en cours", "En cours d'enregistrement ...", "");
    registerAPI();
  }
  else {
    setUpModal("Erreur", "Merci de lire et d'approuver les conditions générales d'utilisation.", "");
  }
}
function addRoleButton(){
  setRole(
  document.getElementById("changeRoleEmailText").value,
  document.getElementById("changeRoleRoleButton").value,
  "POST");
}
function removeRoleButton(){
  setRole(
  document.getElementById("changeRoleEmailText").value,
  document.getElementById("changeRoleRoleButton").value,
  "DELETE");
}
function addItemIndex(){
    document.getElementById("itemAjoutContainer").classList.add('containerShow');
    document.getElementById("searchContainer").classList.remove('containerShow');
    getList('enseignes');
    getList('categories');
    getList('emplacements');
}
function addBrandIndex(){
    document.getElementById("brandAjoutContainer").classList.add('containerShow');
    document.getElementById("searchContainer").classList.remove('containerShow');
}
/* UI Utilities */
function listRoleButton(data){
    getLog('listRoleButton data.innerHTML', data.innerHTML);
    getLog('listRoleButton value', data.getAttribute("value"));
    document.getElementById("changeRoleRoleButton").value = data.getAttribute("value");
    document.getElementById("changeRoleRoleButton").innerHTML = data.innerHTML;
}
function getChangePassword(){
  changePassword(document.getElementById(
  "changePasswordText").value);
}
function getResetPassword(){
  resetPassword(document.getElementById(
  "resetPasswordText").value);
}
function listClick(list){
  getLog("listClick list", list.innerHTML);
  getLog("listClick list is", list.getAttribute("id"));
  button = document.getElementById(list.getAttribute("but"));
  getLog("listClick button", button.innerHTML);
  getLog("listClick button id", button.getAttribute("id"));
  button.innerHTML = list.innerHTML;
  //list.siblings('li').removeClass('active');
  //list.addClass('active');
}
function setUpModal(title, info, infoLink){
  updateModal(title, info, infoLink);
  showModal();
}
function updateModal(title, info, infoLink){
  if (title != false){
    document.getElementById("ModalTitle").innerHTML = title;
  }
  document.getElementById("messageInfo").innerHTML = info;
  document.getElementById("messageInfoLink").innerHTML = infoLink;
}
function respToHTML(response, defaultText){
  this.resp = '';
  if (response){
    this.resp = JSON.parse(response);
    this.html = '';
    if (this.resp.hasOwnProperty('response')){
      if (this.resp["response"].hasOwnProperty('text')){
        this.html = this.html + this.resp["response"]["text"];
      }
      else if (this.resp["response"].hasOwnProperty('messages')){
        this.html = this.html + this.resp["response"]["text"];
      }
      else if (this.resp["response"].hasOwnProperty('message')){
        this.html = this.html + this.resp["response"]["text"];
      }
    }
    else
    {
      this.html = this.html + defaultText;
    }
    return this.html;
  }
  return defaultText
}
function callbackOnAPI(func, status, response, successAction, errorAction, nonModalUpdate){
  if (status >= 200 && status < 400) {
    if (!nonModalUpdate) {
      var modal_html = respToHTML(response, "Opération effectuée");
      getLog("callbackOnAPI modal message", modal_html)
      updateModal(false, modal_html, '');
    }
    if (successAction){
      successAction(JSON.parse(response));
    }
  }
  else{
    getLog(func + 'response error', response);
    var modal_html = respToHTML(response, "Problème lors du changement");
    updateModal(false, modal_html, '');
    if (errorAction){
      errorAction(JSON.parse(response));
    }
  }
}

/* API Calls */
function checkRole(role, action){
    var token = getCookie("user_token");
    var return_bool = false;
    var request = new XMLHttpRequest();
    request.open('GET', getServerURL() + "/auth/check_role/" + role, true);
    request.setRequestHeader('Authentication-Token', token);
    request.onload = function() {
      var resp = JSON.parse(this.response)
      if (this.status >= 200 && this.status < 400) {
        getLog('changeRole ' + role + ' success', resp);
        if (resp == true)
        {
          action();
          return_bool = true;
        }
        else
        {
          return_bool = false;
        }
      }
      else {
        return_bool = false;
      }
    }
    request.onerror = function() {
      return_bool = false;
    };
  request.send();
  return return_bool;
}
function setRole(email, role, method){
  setUpModal("Enregistrement en cours", "Enregistrement du rôle en cours", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  var json = JSON.stringify({email: email, role: role})
  request.open(method, getServerURL() + "/auth/change_role", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', "application/json");
  request.onload = function() {
    callbackOnAPI('setRole', this.status, this.response);
  }
  request.onerror = function() {
    getLog('setRole response error', this.response);
    html = respToHTML(this.response, "Problème lors du changement")
    updateModal(false, html, '')
  };
  request.send(json);
}
function changePassword(newPassword){
  setUpModal("Changement de mot de passe", "Changement en cours", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  var json = JSON.stringify({password: newPassword})
  request.open('PUT', getServerURL() + "/auth/change_password", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  request.onload = function() {
    callbackOnAPI('changePassword', this.status, this.response);
    document.getElementById("changePasswordText").value = "";
  };
  request.onerror = function() {
    getLog('changePassword response error', this.response);
    html = respToHTML(this.response, "Problème lors du changement");
    updateModal(false, html, '');
  };
  request.send(json);
}
function resetPassword(resetEmail){
  setUpModal("Changement de mot de passe", "Changement en cours", "");
  var request = new XMLHttpRequest();
  var json = JSON.stringify({email: resetEmail})
  request.open('POST', getServerURL() + "/auth/reset_password", true);
  request.setRequestHeader('content-type', 'application/json');
  request.onload = function() {
    callbackOnAPI('resetPassword', this.status, this.response);
    document.getElementById("resetPasswordText").value = "";
  };
  request.onerror = function() {
    getLog('resetPassword response error', this.response);
    html = respToHTML(this.response, "Problème lors du changement");
    updateModal(false, html, '');
  };
  request.send(json);
}
function getConditions() {
  setUpModal("Conditions générales d'utilisation", "En cours de chargement ...", "");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/auth/conditions", true);
  request.onload = function() {
    callbackOnAPI('getConditions', this.status, this.response);
  }
  request.onerror = function() {
    getLog('getConditions response error', this.response);
    html = respToHTML(this.response, "Erreur lors du chargement des conditions générales d'utilisation");
    updateModal(false, html, '');
  };
  request.send();
}
function loginAPI() {
  var json = JSON.stringify({
    email: document.getElementById("loginEmail").value,
    password: document.getElementById("loginPassword").value
  })
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/auth/login", true);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
   setCookie("user_id",resp['response']['data']['user_id'],1);
   setCookie("user_username",resp['response']['data']['user_username'],1);
   setCookie("user_email", document.getElementById("loginEmail").value,1);
   setCookie("user_token",resp['response']['data']['auth_token'],1);
   var today_time = Date.now();
   setCookie("user_token_date",today_time,1);
   document.getElementById("loginPassword").value = '';
   loginCheck();
   getIndex();
   hideModal();
  }
  request.onload = function() {
    callbackOnAPI('loginAPI', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('loginAPI response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'authentification");
    updateModal(false, html, '');
  }
  request.send(json);
}
function registerAPI() {
  var json = JSON.stringify({
    email: document.getElementById("registerEmail").value,
    password: document.getElementById("registerPassword").value,
    confirm_link: getConfirmURL()
  })
  funcOnSuccess = function(resp){
     document.getElementById("registerPassword").value = '';
  }
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/auth/register", true);
  request.setRequestHeader('content-type', 'application/json')
  request.onload = function() {
    callbackOnAPI('registerAPI', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('registerAPI response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getItem(item_id) {
  var token = getCookie("user_token");
  getLog("getItem item_id", item_id);
  document.getElementById("adresseTextItem").innerHTML = '';
  document.getElementById("adresseTextItem").value = '';
  document.getElementById("avantageTextItem").innerHTML = '';
  document.getElementById("avantageTextItem").value = '';
  document.getElementById("itemID").value = '';
  document.getElementById("commentTextItem").text = '';
  document.getElementById("commentTextItem").innerHTML = "";
  document.getElementById("commentTextItem").value = "";
  document.getElementById("commentItemUserID").value = '';
  document.getElementById("validationItemSwitch").checked = false;
  document.getElementById("emplacementsGareSwitch").checked = false;
  var list= [
    'departements',
    'enseignes',
    'categories',
    'emplacements',
    'statuts'
  ];
  list.forEach(function(route, index, array) {
    document.getElementById(route + "NomItem").innerHTML = '';
    document.getElementById(route + "NomItem").value = '';
  });

  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items?id="+item_id, true);
  request.setRequestHeader('Authentication-Token', token)
  funcOnSuccess = function(resp) {
      getLog("getItem success", "In")
      getItemComments(item_id);
      if (document.getElementById("adresseTextItem")){
        document.getElementById("adresseTextItem").value = resp["response"]["data"][0]["location_address"];
        document.getElementById("adresseTextItem").innerHTML = resp["response"]["data"][0]["location_address"];
      }
      if (document.getElementById("avantageTextItem")){
        document.getElementById("avantageTextItem").value = resp["response"]["data"][0]["avantage_text"];
        document.getElementById("avantageTextItem").innerHTML = resp["response"]["data"][0]["avantage_text"];
      }
      if(document.getElementById("itemID")){
        document.getElementById("itemID").value = resp["response"]["data"][0]["id"];
      }
      getFromID(resp["response"]["data"][0]["category_id"], "categories");
      getFromID(resp["response"]["data"][0]["status_id"], "statuts");
      if (resp["response"]["data"][0]["departement_id"]){
        getFromID(resp["response"]["data"][0]["departement_id"], "departements");
      }
      else{
        document.getElementById("departementsNomItem").innerHTML = resp["response"]["data"][0]["departement_name"];
        document.getElementById("departementsNomItem").value = resp["response"]["data"][0]["departement_name"];
      }
      if (resp["response"]["data"][0]["location_id"]){
        getFromID(resp["response"]["data"][0]["location_id"], "emplacements");
        document.getElementById("emplacementsID").value = resp["response"]["data"][0]["location_id"];
        document.getElementById("emplacementsID").text = resp["response"]["data"][0]["location_id"];
        document.getElementById("emplacementsID").innerHTML = resp["response"]["data"][0]["location_id"];
      }
      else {
        document.getElementById("emplacementsNomItem").innerHTML = resp["response"]["data"][0]["location_city"];
        document.getElementById("emplacementsNomItem").value = resp["response"]["data"][0]["location_city"];
        document.getElementById("emplacementsEditItem").classList.add('d-none');
        document.getElementById("saveButtonLocation").classList.add('d-none');
      }
      getFromID(resp["response"]["data"][0]["brand_id"], "enseignes");
      getItemMap(resp["response"]["data"][0]["lat_float"], resp["response"]["data"][0]["lon_float"]);
  }
  request.onload = function() {
    getLog("getItem onload", this.response);
    callbackOnAPI('getItem', this.status, this.response, funcOnSuccess);
  };
  request.onerror = function() {
    setUpModal("Problème", "", "");
    getLog('getItem response error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données");
    updateModal(false, html, '');
  };
  request.send();
}
function saveItem(){
  if(document.getElementById("itemID").value){
    setItem();
  }
}
function setItem(){
  var json = JSON.stringify({
    id: document.getElementById("itemID").value,
    description: document.getElementById("avantageTextItem").value,
    adresse: document.getElementById("adresseTextItem").value,
    categorie: document.getElementById("categoriesNomItem").innerHTML,
    statut: document.getElementById("statutsNomItem").innerHTML,
    dept: document.getElementById("departementsNomItem").innerHTML,
    ville: document.getElementById("emplacementsNomItem").innerHTML,
    enseigne: document.getElementById("enseignesNomItem").innerHTML,
  })
  setUpModal("Enregistrement en cours", "Enregistrement de l'avantage en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('PUT', getServerURL() + "/items", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    searchItemsOnKeypress();
    getLog("setItem",JSON.stringify(resp));
  }
  request.onload = function() {
    callbackOnAPI('setItem', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('setItem response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function createItem(){
  var json = JSON.stringify({
    description: document.getElementById("avantageTextItemAjout").value,
    adresse: document.getElementById("adresseTextItemAjout").value,
    categorie: document.getElementById("categoriesNomItemAjout").innerHTML,
    ville: document.getElementById("emplacementsNomItemAjout").innerHTML,
    enseigne: document.getElementById("enseignesNomItemAjout").innerHTML,
  })
  getLog("createItem json",json);
  setUpModal("Enregistrement en cours", "Enregistrement de l'avantage en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/items", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    searchItemsOnKeypress();
    document.getElementById("adresseTextItemAjout").value = "";
    document.getElementById("avantageTextItemAjout").value = "";
    document.getElementById("emplacementsNomItemAjout").innerHTML = "";
    document.getElementById("categoriesNomItemAjout").innerHTML = "";
    document.getElementById("enseignesNomItemAjout").innerHTML = "";
    getLog("createItem",JSON.stringify(resp));
  }
  request.onload = function() {
    callbackOnAPI('createItem', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('createItem response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getFromID(id, route) {
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/" + route + "?id=" + id, true);
  request.setRequestHeader('Authentication-Token', token);
  funcOnSuccess = function(resp){
    route = resp["response"]["data"][0]['type'];
    getLog("getFromID route", route);
    if (document.getElementById(route + "Nom")){
      document.getElementById(route + "Nom").innerHTML = resp["response"]["data"][0]["nom"];
      document.getElementById(route + "Nom").value = resp["response"]["data"][0]["nom"];
    }
    if (document.getElementById(route + "NomItem")){
      document.getElementById(route + "NomItem").innerHTML = resp["response"]["data"][0]["nom"];
      document.getElementById(route + "NomItem").value = resp["response"]["data"][0]["nom"];
    }
    if (document.getElementById(route + "Code")){
      document.getElementById(route + "Code").innerHTML = resp["response"]["data"][0]["code"];
      document.getElementById(route + "Code").value = resp["response"]["data"][0]["code"];
    }
    if (document.getElementById(route + "CodeItem")){
      document.getElementById(route + "CodeItem").innerHTML = resp["response"]["data"][0]["code"];
      document.getElementById(route + "CodeItem").value = resp["response"]["data"][0]["code"];
    }
    if (document.getElementById(route + "ID")){
      document.getElementById(route + "ID").value = resp["response"]["data"][0]["id"];
    }
    if (resp["response"]["data"][0]["gare"]){
      if (resp["response"]["data"][0]["gare"] == true){
        document.getElementById("emplacementsGareSwitch").checked = true;
      }
    }
  }
  request.onload = function() {
    this.response.route = route;
    callbackOnAPI('getFromID', this.status, this.response, funcOnSuccess);
  };
  request.onerror = function() {
    setUpModal("Problème", "", "");
    getLog('getFromID response error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données");
    updateModal(false, html, '');
  };
  request.send();
}
function setDept(){
 setGeneric(
 'departements');
}
function saveBrand(){
 saveGeneric(
 'enseignes');
}
function setCategory(){
 setGeneric(
 'categories');
}
function setLocation(){
 setGeneric(
 'emplacements');
}
function setStatus(){
 setGeneric(
 'statuts');
}
function saveGeneric(route){
  var id = document.getElementById(route + "ID").value;
  getLog("id brand", id);
  if (id){
    setGeneric(route);
  }
  else{
    createGeneric(route);
  }
}
function createGeneric(route){
  getLog('createGeneric routeNom', document.getElementById(route + "NomAjout").value)
  var json = JSON.stringify({
     code: document.getElementById(route + "NomAjout").value,
     nom: document.getElementById(route + "NomAjout").value,
   });
   getLog('createGeneric json', json);
  setUpModal("Enregistrement en cours", "Enregistrement en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/" + route, true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    if (document.getElementById("enseignesNomAjout")){
        document.getElementById("enseignesNomAjout").value = "";
    }
    if (    document.getElementById("enseignesCodeAjout")){
        document.getElementById("enseignesCodeAjout").value = "";
    }
    searchItemsOnKeypress();
  }
  request.onload = function() {
    callbackOnAPI('createGeneric', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('createGeneric response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function setGeneric(route){
  getLog('setGeneric routeNom', document.getElementById(route + "Nom").value)
  var json = JSON.stringify({
     id: document.getElementById(route + "ID").value,
     code: document.getElementById(route + "Code").value,
     nom: document.getElementById(route + "Nom").value,
   });
   if (route == "emplacements") {
    var json = JSON.stringify({
       id: document.getElementById(route + "ID").value,
       code: document.getElementById(route + "Code").value,
       nom: document.getElementById(route + "Nom").value,
       gare: document.getElementById("emplacementsGareSwitch").checked,
     });
   }
  setUpModal("Enregistrement en cours", "Enregistrement en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('PUT', getServerURL() + "/" + route, true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    searchItemsOnKeypress();
  }
  request.onload = function() {
    callbackOnAPI('setGeneric', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('setGeneric response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getList(path) {
  getLog("getList in", path);
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/" + path, true);
  request.setRequestHeader('Authentication-Token', token);

  request.onload = function() {
    /*TODO Use callbackOnAPI and make it work with a modo user*/
    if (this.status >= 200 && this.status < 400) {
      var resp = JSON.parse(this.response);
      var list_html = "";
      var list_html_ajout = "";
      var list_url = path + 'List';
      var list_url_ajout = list_url + 'Ajout';
      getLog("list_url_ajout",list_url_ajout)
      var list_content = document.getElementById(list_url);
      var list_content_ajout = document.getElementById(list_url_ajout);
      var type = resp["response"]["data"][0]["type"]
      resp["response"]["data"].forEach(function(item, index){
        var button = type + "NomItem";
        list_html = list_html + "<li but='"+ button +"' onclick='listClick(this)' value='" + item["id"] + "' class='dropdown-item'>" + item["nom"] + "</li>";
        var button_ajout = type + "NomItemAjout";
        getLog("button_ajout",button_ajout)
        list_html_ajout = list_html_ajout + "<li but='"+ button_ajout +"' onclick='listClick(this)' value='" + item["id"] + "' class='dropdown-item'>" + item["nom"] + "</li>";
      });
      if(list_content){
        list_content.innerHTML = list_html;
      }
      if(list_content_ajout){
        list_content_ajout.innerHTML = list_html_ajout;
       }
    }
    else{
      html = respToHTML(this.response, "Erreur lors de la récupération des données.");
      updateModal(false, html, '');
    }
  }
  request.onerror = function() {
    getLog('getList error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données.");
    updateModal(false, html, '');
  }
  request.send();
}
function exportItemsCsv(){
  setUpModal("Export du fichier", '', '');

  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items", true);
  request.setRequestHeader('Authentication-Token', token)
  request.setRequestHeader('accept', 'text/csv')
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      var file = new Blob([this.response], {type: 'text/csv'});
      document.getElementById("messageInfoLink").setAttribute("href", URL.createObjectURL(file));
      document.getElementById("messageInfoLink").setAttribute("download", "export.csv");
      document.getElementById("messageInfoLink").innerHTML = "Cliquer ici pour le télécharger";
    }
    else {
      document.getElementById("messageInfo").innerHTML = "Erreur lors de la création du fichier";
    }
  }
  request.onerror = function() {
    getLog('exportItemsCsv error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données.");
    updateModal(false, html, '');
  }
  request.send();
}
function confirmToken(token) {
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/auth/confirm/" + token, true);
  funcOnSuccess = function(resp){
    document.getElementById("confirmText").innerHTML = "Compte confirmé.";
    window.history.pushState({page: 'confirm'}, 'Détail', '/index.html');
  };
  funcOnError = function(resp){
    document.getElementById("confirmText").innerHTML = "Le lien est invalide ou expiré.";
    window.history.pushState({page: 'confirm'}, 'Détail', '/index.html');
  }
  request.onload = function() {
    callbackOnAPI('confirmToken', this.status, this.response, funcOnSuccess, funcOnError);
  }
  request.onerror = function() {
    getLog('confirmToken error', this.response);
    html = respToHTML(this.response, "Erreur lors de la confirmation.");
    updateModal(false, html, '');
  }
  request.send();
}

// Items
function searchItemsClean(){
  document.getElementById(
  "tableBody").innerHTML = '';
}
function searchItemsOnKeypress(){
  var searchText = document.getElementById("searchText").value;
  getLog("searchItemsOnKeypress: ", searchText);
  clearTimeout(timerId);
  var len = searchText.length;
  if (len < 3)
  {
    searchItemsClean();
  }
  else {
    document.getElementById("tableBody").textContent = "Chargement";
    timerId = setTimeout(searchItemsTable, 1000);
  }
}
function searchItemsTable() {
  var searchText = document.getElementById("searchText").value;
  getLog("searchItemsTable: ", searchText);
  var token = getCookie("user_token");

  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items?recherche="+searchText, true);
  //request.open('GET', getServerURL() + "/items?par_statuts=confirmes&recherche="+searchText, true);
  request.setRequestHeader('Authentication-Token', token)
  funcOnSuccess = function(resp){
      this.html = '';
      document.getElementById("tableBody").innerHTML = '';
      this.html = this.html + "<table class='table table-bordered'>";
      this.html = this.html + "<thead class='thead'>";
      this.html = this.html + "<tr>";
      this.html = this.html + "<th scope='col'>Département</th>";
      this.html = this.html + "<th scope='col'>Ville</th>";
      this.html = this.html + "<th scope='col'>Catégorie</th>";
      this.html = this.html + "<th scope='col'>Enseigne</th>";
      this.html = this.html + "<th scope='col'></th>";
      this.html = this.html + "</tr>";
      this.html = this.html + "</thead>";
      this.html = this.html + "<tbody>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = this.html;
      getLog("searchItemsTable this.response", resp);
      resp["response"]["data"].forEach(function(item, index){
        rank = index + 1;
        pol_s = "";
        pol_e = "";
        if (item["status_code"] == "INVALIDE-01"){
          pol_s = "<s>";
          pol_e = "</s>";
        }
        if (item["status_code"] != "CONFIRME-01"){
          this.html = this.html + "<tr style='opacity: 0.4;'>";
        }
        else {
          this.html = this.html + "<tr>";
        }
        if (item["departement_code"]){
          this.html = this.html + "<td>" + pol_s + item["departement_code"] + " - "+ item["departement_name"] + pol_e + "</td>";
        }
        else {
          this.html = this.html + "<td>" + pol_s + "Inconnu" + pol_e + "</td>";
        }
        this.html = this.html + "<td>" + pol_s + item["location_city"] + pol_e + "</td>";
        this.html = this.html + "<td>" + pol_s + item["category_name"] + pol_e + "</td>";
        this.html = this.html + "<td>" + pol_s + item["brand_name"] + pol_e + "</td>";
        this.html = this.html + "<td>" + pol_s + "<a class='kinda-link text-primary' onclick='loadItemPage("+ item["id"] +")'>" + "<i class='icons-search' aria-hidden='true'></i>" + "</a>" + pol_e + "</td>";
        this.html = this.html + "</tr>";
      });
      this.html = this.html + "</tbody>";
      this.html = this.html + "</table>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = this.html;
  };
  funcOnError = function(resp){
      this.html = '';
      this.html = this.html + "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
      this.html = this.html + "<h6 class='h6 font-weight-normal text-center text-danger' >Problème de connexion au serveur</h6>";
      this.html = this.html + "</div>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = html;
  }
  request.onload = function() {
    callbackOnAPI('searchItemsTable', this.status, this.response, funcOnSuccess, funcOnError, true);
  };
  request.onerror = function() {
    var html = '';
    html = html + "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
    html = html + "<h6 class='h6 font-weight-normal text-center text-danger' >Problème de traitement de la page</h6>";
    html = html + "</div>";
    document.getElementById("tableBody").innerHTML = '';
    document.getElementById("tableBody").innerHTML = html;
  };
  request.send();
}
function commentItemSave() {
  setUpModal("Commentaire", "Envoi du commentaire en cours");
  var item_id = document.getElementById("itemID").value;
  var token = getCookie("user_token");
  var methodRequest = "PUT"
  var json = ""
  var validation = document.getElementById("validationItemSwitch").checked;
  var commentID = document.getElementById("commentItemUserID").value;
  getLog("commentItemSave commentID", commentID)
  if (commentID == null){
    methodRequest = "POST";
    json = JSON.stringify({
       message: document.getElementById("commentTextItem").value,
       validation: validation,
     });
  }
  else if (commentID == "") {
    methodRequest = "POST";
    json = JSON.stringify({
       message: document.getElementById("commentTextItem").value,
       validation: validation,
     });
  }
  else {
    methodRequest = "PUT";
    json = JSON.stringify({
       id: document.getElementById("commentItemUserID").value,
       message: document.getElementById("commentTextItem").value,
       validation: validation,
     });
  }
  getLog("commentItemSave json", json);
  var request = new XMLHttpRequest();
  request.open(methodRequest, getServerURL() + "/items/"+ item_id +"/commentaires/", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    getItemComments(item_id);
  };
  funcOnError = function(resp){
    
  }
  request.onload = function() {
    callbackOnAPI('commentItemSave', this.status, this.response, funcOnSuccess, funcOnError);
  }
  request.onerror = function() {
    getLog('commentItemSave error', this.response);
    html = respToHTML(this.response, "Erreur lors de la sauvegarde.");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getItemComments(item_id) {
  getLog('getItemComments', "In");
  var request = new XMLHttpRequest();
  var token = getCookie("user_token");
  request.open('GET', getServerURL() + "/items/"+ item_id +"/commentaires/", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){

  };
  funcOnError = function(resp){
    
  }
  request.onload = function(){
    if (this.status >= 200 && this.status < 400) {
    document.getElementById("commentTextItem").text = '';
    document.getElementById("commentTextItem").innerHTML = '';
    document.getElementById("commentTextItem").value = "";
    document.getElementById("validationItemSwitch").checked = false;
    var resp = JSON.parse(this.response);
    getLog('getItemComments response success', this.response);

    if (resp["response"]["data"]["nombre"] > 0){
      var html_valid = "";
      html_valid = html_valid + "<div class='col-12 mb-3 mt-3'>";
      html_valid = html_valid + "<h5 class='h5 font-weight-normal'>";
      if (resp["response"]["data"]["validations"] == 0){
        html_valid = html_valid + "Cet avantage n'a pas été validé.";
      }
      else {
        html_valid = html_valid + "Cet avantage est validé par ";
        pourcentage = (resp["response"]["data"]["validations"] / resp["response"]["data"]["nombre"]) * 100;
        html_valid = html_valid + pourcentage + " % des contributeurs (";
        html_valid = html_valid + resp["response"]["data"]["nombre"];
        if (resp["response"]["data"]["nombre"] == 1){
            html_valid = html_valid + " contribution).\n";
        }
        else {
            html_valid = html_valid + " contributions).\n";
        }
        html_valid = html_valid + "<br>";
        html_valid = html_valid + "Dernière validation: ";
        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var date = new Date(resp["response"]["data"]["derniere_validation"]);
        html_valid = html_valid + date.toLocaleString('fr-FR', options);
      }
      html_valid = html_valid + "</h5>";
      html_valid = html_valid + "</div>";
      document.getElementById("validationCount").classList.remove('d-none');
      document.getElementById("validationCount").innerHTML = '';
      document.getElementById("validationCount").innerHTML = html_valid;
    }
    else {
      document.getElementById("validationCount").classList.add('d-none');
    }

    var html = '';
    resp["response"]["data"]["commentaires"].forEach(function(item, index){
        if (item["cree_par_id"] == getCookie("user_id"))
        {
          document.getElementById("commentTextItem").text = '';
          document.getElementById("commentTextItem").innerHTML = '';
          document.getElementById("commentTextItem").value = "";
          document.getElementById("commentTextItem").value = item["message"];
          document.getElementById("commentItemUserID").value = item["id"];
          if (item["validation"] == true) {
            document.getElementById("validationItemSwitch").checked = true;
          };
          getLog('getItemComments meme id', item["message"]);
        }
        else if (item["message"] != "")
        {
          var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
          var date = new Date(item["modifie_le"]);
          html = html + "<div class='col-12 mb-3 mt-3 border'>";
          html = html + "<h6 class='h6 font-weight-normal'>Posté par ";
          html = html + item["cree_par"];
          html = html + " le ";
          html = html + date.toLocaleString('fr-FR', options);
          if (item["validation"] == true){
            html = html + " - Valide cet avantage ";
          }
          else {
            html = html + " - Ne valide pas cet avantage ";
          }
          html = html + "</h6>";
          html = html + "<div class='mb-3 mt-3'>";
          html = html + "<textarea type='text' class='form-control bg-white stretchy' readonly>";
          html = html + item["message"];
          html = html + "</textarea>";
          html = html + "</div>";
          html = html + "</div>";
          getLog('getItemComments pas meme id', item["message"]);
        }
    });
    document.getElementById("commentsAllItem").innerHTML = '';
    document.getElementById("commentsAllItem").innerHTML = html;
  }
  else {
    updateModal("Commentaire", "Erreur lors de la recuperation des donnees.","")
  }
    /*callbackOnAPI('getItemComments', this.status, this.response, funcOnSuccess, funcOnError);*/
  }
  request.onerror = function() {
    getLog('getItemComments error', this.response);
    html = respToHTML(this.response, "Erreur lors de la recuperation des donnees.");
    updateModal(false, html, '');
  }
  request.send();
}
function getItemMap(lat, lon){

  var container = L.DomUtil.get('itemMapId');
  if(container != null){
    container._leaflet_id = null;
  }
  if (lat){
    if (lon){
      document.getElementById("itemMap").classList.remove('d-none');
    	var mymap = L.map('itemMapId').setView([lat, lon], 16);
      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2lsaXIiLCJhIjoiY2l4bTJoZ2plMDBnaDMxbnN3OXYzcmsydCJ9.tFQwarFT13hQQgB-QFVlNQ', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
          '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
          'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
      }).addTo(mymap);

      var marker = L.marker([lat, lon]).addTo(mymap);
    }
    else{
      document.getElementById("itemMap").classList.add('d-none');
    }
  }
  else{
      document.getElementById("itemMap").classList.add('d-none');
  }
}