/* Global Variables */
var timerId;

/* Utilities */
function showModal(){
  /* TODO Migrate from JQuery */
  $("#Modal").modal();
}
function hideModal(){
  /* TODO Migrate from JQuery */
  $('#Modal').modal('hide');
}
function getLog(title, message){
  if (getDebug()){
      console.log(title + ' : ' + message);
  }
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function deleteCookie(cname) {
    var expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC";
    document.cookie = cname + "=" + ";" + expires + ";path=/";
}
function obtenirParametre (sVar) {
  return unescape(window.location.search.replace(
  new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

/* Initial SetUp */
function setUpIndex(){
  fetchNavBar();
  fetchFooter();
  fetchModal();
  fetchContent();
  /* TODO Migrate from JQuery */
  $(window).on('popstate', function(){loadHistory(event)});
}
function fetchNavBar(){
  fetch("pages/nav-bar.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data) {
    document.getElementById("nav-placeholder").innerHTML = data;
    var username = getCookie("user_username");
    if(username != ""){
      document.getElementById("navLogin").textContent = username;
    }
  })
}
function fetchFooter(){
  fetch("pages/footer.html")
  .then(function(response){
    return response.text()
  })
  .then(function(data){
    document.getElementById("footer-placeholder").innerHTML = data
  })
}
function fetchModal(){
  fetch("pages/modal.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("modal-placeholder").innerHTML = data
  })
}
function fetchContent(){
  fetch("pages/dept.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("deptContainer").innerHTML = data
  })
  fetch("pages/brand.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("brandContainer").innerHTML = data
  })
  fetch("pages/category.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("categoryContainer").innerHTML = data
  })
  fetch("pages/location.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("locationContainer").innerHTML = data
  })
  fetch("pages/status.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("statusContainer").innerHTML = data
  })
  fetch("pages/item.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("itemContainer").innerHTML = data
  })
  .then(function(data){
    testItem();
  })
  fetch("pages/login.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("loginContainer").innerHTML = data
  })
  fetch("pages/profil.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("profilContainer").innerHTML = data
  })
  fetch("pages/confirm.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("invalidContainer").innerHTML = data
  })
  .then(function(data){
    testConfirm();
  })
  fetch("pages/connectionNeeded.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
    document.getElementById("connectionNeededContainer").innerHTML = data
  })
  fetch("pages/searchItems.html")
  .then(function(response) {
    return response.text()
  })
  .then(function(data){
   document.getElementById("searchContainer").innerHTML = data
  })

  var username = getCookie("user_username");
  if(username == ""){
      document.getElementById("connectionNeededContainer").classList.add('containerShow');
  }
  else{
      document.getElementById("searchContainer").classList.add('containerShow');
  }

}
function cleanContainers(){
  document.getElementById("deptContainer").classList.remove('containerShow');
  document.getElementById("brandContainer").classList.remove('containerShow');
  document.getElementById("categoryContainer").classList.remove('containerShow');
  document.getElementById("locationContainer").classList.remove('containerShow');
  document.getElementById("statusContainer").classList.remove('containerShow');
  document.getElementById("itemContainer").classList.remove('containerShow');
  document.getElementById("loginContainer").classList.remove('containerShow');
  document.getElementById("profilContainer").classList.remove('containerShow');
  document.getElementById("invalidContainer").classList.remove('containerShow');
  document.getElementById("connectionNeededContainer").classList.remove('containerShow');
  document.getElementById("searchContainer").classList.remove('containerShow');
  document.getElementById("connectionNeededContainer").classList.remove('containerShow');
}
function testConfirm(){
    var confirm_token = obtenirParametre("confirm_token");
    if (confirm_token != "") {
      cleanContainers();
      document.getElementById("invalidContainer").classList.add('containerShow');
      confirmToken(confirm_token);
    } else {
      getLog("testConfirm", "Invalid");
    }
}

function testItem(){
    var item = obtenirParametre("item");
    if (item != "") {
      cleanContainers();
      document.getElementById("itemContainer").classList.add('containerShow');
      loadItemPage(item);
    } else {
      getLog("testConfirm", "Invalid");
    }
}

/* Pages */
function loginCheck(){
  var username = getCookie("user_username");
  if(username == ""){
    document.getElementById("connectionNeededContainer").classList.add('containerShow');
    return false;
  }
  else {
    var username = getCookie("user_username");
    if(username != ""){
      if (document.getElementById("navLogin")){
        document.getElementById("navLogin").textContent = username;
      }
    }
    checkRole("admin", loginAdminSetUp);
    checkRole("modo", loginModoSetUp);
    return true;
  }
}
function getIndex(){
  getLog("goIndex", "In");
  cleanContainers();
  var loginOK = loginCheck();
  if (loginOK == true) {
      document.getElementById("searchContainer").classList.add('containerShow');
  }
}
function getLogin(){
  getLog("goLogin", "In");
  window.history.pushState({page: 'login'}, 'Détail', '');
  cleanContainers();
  var loginOK = loginCheck();
  if (loginOK == true) {
    document.getElementById("profilContainer").classList.add('containerShow');
  }
  else{
    document.getElementById("connectionNeededContainer").classList.remove('containerShow');
    document.getElementById("loginContainer").classList.add('containerShow');
  }
}
function getLogout(){
  logoutSetUp();
  document.getElementById("navLogin").textContent = "Connexion";
  deleteCookie("user_id");
  deleteCookie("user_username");
  deleteCookie("user_token");
  deleteCookie("user_email");
  deleteCookie("user_token_date");
  getIndex();
}
function loginAdminSetUp(){
  document.getElementById(
  "changeRoleForm").classList.remove('d-none');
}
function loginModoSetUp(){

  document.getElementById("exportItemsCsv").classList.remove('d-none');
  document.getElementById("adresseTextItem").readOnly = false;
  document.getElementById("avantageTextItem").readOnly = false;
  document.getElementById("emplacementsNomItem").disabled = false;
  document.getElementById("departementsNomItem").disabled = false;
  document.getElementById("statutsNomItem").disabled = false;
  document.getElementById("categoriesNomItem").disabled = false;
  document.getElementById("enseignesNomItem").disabled = false;
  document.getElementById("saveButtonItem").classList.remove('d-none');

  getList('enseignes');
  getList('emplacements');
  getList('departements');
  getList('categories');
  getList('statuts');

  document.getElementById("departementsNom").readOnly = false;
  document.getElementById("departementsCode").readOnly = false;
  document.getElementById("saveButtonDept").classList.remove('d-none');

  document.getElementById("enseignesNom").readOnly = false;
  document.getElementById("enseignesNom").readOnly = false;
  document.getElementById("saveButtonBrand").classList.remove('d-none');

  document.getElementById("categoriesNom").readOnly = false;
  document.getElementById("categoriesNom").readOnly = false;
  document.getElementById("saveButtonCategory").classList.remove('d-none');

  document.getElementById("emplacementsNom").readOnly = false;
  document.getElementById("emplacementsCode").readOnly = false;
  document.getElementById("saveButtonLocation").classList.remove('d-none');

  document.getElementById("statutsNom").readOnly = false;
  document.getElementById("statutsCode").readOnly = false;
  document.getElementById("saveButtonStatus").classList.remove('d-none');
}
function logoutSetUp(){
  document.getElementById("exportItemsCsv").classList.add('d-none');
  document.getElementById("changeRoleForm").classList.add('d-none');

  document.getElementById("adresseTextItem").readOnly = true;
  document.getElementById("avantageTextItem").readOnly = true;
  document.getElementById("departementsNomItem").disabled = true;
  document.getElementById("emplacementsNomItem").disabled = true;
  document.getElementById("statutsNomItem").disabled = true;
  document.getElementById("categoriesNomItem").disabled = true;
  document.getElementById("enseignesNomItem").disabled = true;
  document.getElementById("saveButtonItem").classList.add('d-none');

  document.getElementById("departementsNom").readOnly = true;
  document.getElementById("departementsCode").readOnly = true;
  document.getElementById("saveButtonDept").classList.add('d-none');

  document.getElementById("enseignesNom").readOnly = true;
  document.getElementById("enseignesNom").readOnly = true;
  document.getElementById("saveButtonBrand").classList.add('d-none');

  document.getElementById("categoriesNom").readOnly = true;
  document.getElementById("categoriesNom").readOnly = true;
  document.getElementById("saveButtonCategory").classList.add('d-none');

  document.getElementById("emplacementsNom").readOnly = true;
  document.getElementById("emplacementsCode").readOnly = true;
  document.getElementById("saveButtonLocation").classList.add('d-none');

  document.getElementById("statutsNom").readOnly = true;
  document.getElementById("statutsCode").readOnly = true;
  document.getElementById("saveButtonStatus").classList.add('d-none');
}

function loadHistory(event){
    getLog("loadHstory","location: " + document.location + ", state: " + JSON.stringify(event.state));
    getIndex();
}

function loadItemPage(id){
  loadPage(
  "item", id);
  window.history.pushState({page: 'item'}, 'Détail', '/index.html?item='+id);
}
function loadDeptPage(id){
  loadPage(
  "dept", id);
}
function loadBrandPage(id){
  loadPage(
  "brand", id);
}
function loadCategoryPage(id){
  loadPage(
  "category", id);
}
function loadLocationPage(id){
  loadPage(
  "location", id);
}
function loadStatusPage(id){
  loadPage(
  "status", id);
}
function loadPage(page, id){
  getLog("loadPage page", page);
  getLog("loadPage id", id);
  var loginOK = loginCheck();
  if (loginOK == true) {
    var array = {
      'dept': 'departements',
      'brand': 'enseignes',
      'category': 'categories',
      'location': 'emplacements',
      'status': 'statuts'
    }
    if (page=='item'){
      getItem(id);
    }
    else
    {
      getFromID(id ,array[page])
    }

    container = page + "Container";
    document.getElementById(container).classList.add('containerShow');
    document.getElementById("searchContainer").classList.remove('containerShow');
  }
  else{
    document.getElementById("searchContainer").classList.remove('containerShow');
  }
}

/* CallBack */
function loginButtonClick(){
  setUpModal("Connexion", "Chargement en cours", "")
  loginAPI();
}
function registerButtonClick(){
  if (document.getElementById('conditionsCheck').checked == true){
    setUpModal("Enregistrement en cours", "En cours d'enregistrement ...", "");
    registerAPI();
  }
  else {
    setUpModal("Erreur", "Merci de lire et d'approuver les conditions générales d'utilisation.", "");
  }
}
function addRoleButton(){
  setRole(
  document.getElementById("changeRoleEmailText").value,
  document.getElementById("changeRoleRoleButton").value,
  "POST");
}
function removeRoleButton(){
  setRole(
  document.getElementById("changeRoleEmailText").value,
  document.getElementById("changeRoleRoleButton").value,
  "DELETE");
}

/* UI Utilities */
function listRoleButton(data){
    getLog('listRoleButton data.innerHTML', data.innerHTML);
    getLog('listRoleButton value', data.getAttribute("value"));
    document.getElementById("changeRoleRoleButton").value = data.getAttribute("value");
    document.getElementById("changeRoleRoleButton").innerHTML = data.innerHTML;
}
function getChangePassword(){
  changePassword(document.getElementById(
  "changePasswordText").value);
}
function listClick(list){
  button = document.getElementById(list.getAttribute("but"))
  button.innerHTML = list.innerHTML;
  //list.siblings('li').removeClass('active');
  //list.addClass('active');
}
function setUpModal(title, info, infoLink){
  updateModal(title, info, infoLink);
  showModal();
}
function updateModal(title, info, infoLink){
  if (title != false){
    document.getElementById("ModalTitle").innerHTML = title;
  }
  document.getElementById("messageInfo").innerHTML = info;
  document.getElementById("messageInfoLink").innerHTML = infoLink;
}
function respToHTML(response, defaultText){
  this.resp = '';
  if (response){
    this.resp = JSON.parse(response);
    this.html = '';
    if (this.resp.hasOwnProperty('response')){
      if (this.resp["response"].hasOwnProperty('text')){
        this.html = this.html + this.resp["response"]["text"];
      }
      else if (this.resp["response"].hasOwnProperty('messages')){
        this.html = this.html + this.resp["response"]["text"];
      }
      else if (this.resp["response"].hasOwnProperty('message')){
        this.html = this.html + this.resp["response"]["text"];
      }
    }
    else
    {
      this.html = this.html + defaultText;
    }
    return this.html;
  }
  return defaultText
}
function callbackOnAPI(func, status, response, successAction, errorAction, nonModalUpdate){
  if (status >= 200 && status < 400) {
    if (!nonModalUpdate) {
      var modal_html = respToHTML(response, "Opération effectuée");
      getLog("callbackOnAPI modal message", modal_html)
      updateModal(false, modal_html, '');
    }
    if (successAction){
      successAction(JSON.parse(response));
    }
  }
  else{
    getLog(func + 'response error', response);
    var modal_html = respToHTML(response, "Problème lors du changement");
    updateModal(false, modal_html, '');
    if (errorAction){
      errorAction(JSON.parse(response));
    }
  }
}

/* API Calls */
function checkRole(role, action){
    var token = getCookie("user_token");
    var return_bool = false;
    var request = new XMLHttpRequest();
    request.open('GET', getServerURL() + "/auth/check_role/" + role, true);
    request.setRequestHeader('Authentication-Token', token);
    request.onload = function() {
      var resp = JSON.parse(this.response)
      if (this.status >= 200 && this.status < 400) {
        getLog('changeRole ' + role + ' success', resp);
        if (resp == true)
        {
          action();
          return_bool = true;
        }
        else
        {
          return_bool = false;
        }
      }
      else {
        return_bool = false;
      }
    }
    request.onerror = function() {
      return_bool = false;
    };
  request.send();
  return return_bool;
}
function setRole(email, role, method){
  setUpModal("Enregistrement en cours", "Enregistrement du rôle en cours", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  var json = JSON.stringify({email: email, role: role})
  request.open(method, getServerURL() + "/auth/change_role", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', "application/json");
  request.onload = function() {
    callbackOnAPI('setRole', this.status, this.response);
  }
  request.onerror = function() {
    getLog('setRole response error', this.response);
    html = respToHTML(this.response, "Problème lors du changement")
    updateModal(false, html, '')
  };
  request.send(json);
}
function changePassword(newPassword){
  setUpModal("Changement de mot de passe", "Changement en cours", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  var json = JSON.stringify({password: newPassword})
  request.open('PUT', getServerURL() + "/auth/change_password", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  request.onload = function() {
    callbackOnAPI('changePassword', this.status, this.response);
    document.getElementById("changePasswordText").value = "";
  };
  request.onerror = function() {
    getLog('changePassword response error', this.response);
    html = respToHTML(this.response, "Problème lors du changement");
    updateModal(false, html, '');
  };
  request.send(json);
}
function getConditions() {
  setUpModal("Conditions générales d'utilisation", "En cours de chargement ...", "");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/auth/conditions", true);
  request.onload = function() {
    callbackOnAPI('getConditions', this.status, this.response);
  }
  request.onerror = function() {
    getLog('getConditions response error', this.response);
    html = respToHTML(this.response, "Erreur lors du chargement des conditions générales d'utilisation");
    updateModal(false, html, '');
  };
  request.send();
}
function loginAPI() {
  var json = JSON.stringify({
    email: document.getElementById("loginEmail").value,
    password: document.getElementById("loginPassword").value
  })
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/auth/login", true);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
   setCookie("user_id",resp['response']['data']['user_id'],1);
   setCookie("user_username",resp['response']['data']['user_username'],1);
   setCookie("user_email", document.getElementById("loginEmail").value,1);
   setCookie("user_token",resp['response']['data']['auth_token'],1);
   var today_time = Date.now();
   setCookie("user_token_date",today_time,1);
   document.getElementById("loginPassword").value = '';
   loginCheck();
   getIndex();
   hideModal();
  }
  request.onload = function() {
    callbackOnAPI('loginAPI', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('loginAPI response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'authentification");
    updateModal(false, html, '');
  }
  request.send(json);
}
function registerAPI() {
  var json = JSON.stringify({
    email: document.getElementById("registerEmail").value,
    password: document.getElementById("registerPassword").value,
    confirm_link: getConfirmURL()
  })
  funcOnSuccess = function(resp){
     document.getElementById("registerPassword").value = '';
  }
  var request = new XMLHttpRequest();
  request.open('POST', getServerURL() + "/auth/register", true);
  request.setRequestHeader('content-type', 'application/json')
  request.onload = function() {
    callbackOnAPI('registerAPI', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('registerAPI response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getItem(item_id) {
  var token = getCookie("user_token");
  getLog("getItem item_id", item_id);
  document.getElementById("adresseTextItem").innerHTML = '';
  document.getElementById("adresseTextItem").value = '';
  document.getElementById("avantageTextItem").innerHTML = '';
  document.getElementById("avantageTextItem").value = '';
  document.getElementById("itemID").value = '';
  var list= [
    'departements',
    'enseignes',
    'categories',
    'emplacements',
    'statuts'
  ];
  list.forEach(function(route, index, array) {
    document.getElementById(route + "NomItem").innerHTML = '';
    document.getElementById(route + "NomItem").value = '';
  });

  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items?id="+item_id, true);
  request.setRequestHeader('Authentication-Token', token)
  funcOnSuccess = function(resp) {
      getLog("getItem success", "In");
      if (document.getElementById("adresseTextItem")){
        document.getElementById("adresseTextItem").value = resp["response"]["data"][0]["location_address"];
        document.getElementById("adresseTextItem").innerHTML = resp["response"]["data"][0]["location_address"];
      }
      if (document.getElementById("avantageTextItem")){
        document.getElementById("avantageTextItem").value = resp["response"]["data"][0]["avantage_text"];
        document.getElementById("avantageTextItem").innerHTML = resp["response"]["data"][0]["avantage_text"];
      }
      if(document.getElementById("itemID")){
        document.getElementById("itemID").value = resp["response"]["data"][0]["id"];
      }
      getFromID(resp["response"]["data"][0]["category_id"], "categories");
      getFromID(resp["response"]["data"][0]["status_id"], "statuts");
      getFromID(resp["response"]["data"][0]["departement_id"], "departements");
      getFromID(resp["response"]["data"][0]["location_id"], "emplacements");
      getFromID(resp["response"]["data"][0]["brand_id"], "enseignes");
  }
  request.onload = function() {
    getLog("getItem onload", "In");
    callbackOnAPI('getItem', this.status, this.response, funcOnSuccess);
  };
  request.onerror = function() {
    setUpModal("Problème", "", "");
    getLog('getItem response error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données");
    updateModal(false, html, '');
  };
  request.send();
}
function setItem(){
  var json = JSON.stringify({
    email: document.getElementById("registerEmail").value,
    password: document.getElementById("registerPassword").value,

    id: document.getElementById("itemID").value,
    description: document.getElementById("avantageTextItem").value,
    adresse: document.getElementById("adresseTextItem").value,
    categorie: document.getElementById("categoriesNomItem").innerHTML,
    statut: document.getElementById("statutsNomItem").innerHTML,
    dept: document.getElementById("departementsNomItem").innerHTML,
    ville: document.getElementById("emplacementsNomItem").innerHTML,
    enseigne: document.getElementById("enseignesNomItem").innerHTML,
  })
  setUpModal("Enregistrement en cours", "Enregistrement de l'avantage en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('PUT', getServerURL() + "/items", true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    searchItemsOnKeypress();
    getLog("setItem",JSON.stringify(resp));
  }
  request.onload = function() {
    callbackOnAPI('setItem', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('setItem response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getFromID(id, route) {
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/" + route + "?id=" + id, true);
  request.setRequestHeader('Authentication-Token', token);
  funcOnSuccess = function(resp){
    route = resp["response"]["data"][0]['type'];
    getLog("getFromID route", route);
    if (document.getElementById(route + "Nom")){
      document.getElementById(route + "Nom").innerHTML = resp["response"]["data"][0]["nom"];
      document.getElementById(route + "Nom").value = resp["response"]["data"][0]["nom"];
    }
    if (document.getElementById(route + "NomItem")){
      document.getElementById(route + "NomItem").innerHTML = resp["response"]["data"][0]["nom"];
      document.getElementById(route + "NomItem").value = resp["response"]["data"][0]["nom"];
    }
    if (document.getElementById(route + "Code")){
      document.getElementById(route + "Code").innerHTML = resp["response"]["data"][0]["code"];
      document.getElementById(route + "Code").value = resp["response"]["data"][0]["code"];
    }
    if (document.getElementById(route + "CodeItem")){
      document.getElementById(route + "CodeItem").innerHTML = resp["response"]["data"][0]["code"];
      document.getElementById(route + "CodeItem").value = resp["response"]["data"][0]["code"];
    }
    if (document.getElementById(route + "ID")){
      document.getElementById(route + "ID").value = resp["response"]["data"][0]["id"];
    }
  }
  request.onload = function() {
    this.response.route = route;
    callbackOnAPI('getFromID', this.status, this.response, funcOnSuccess);
  };
  request.onerror = function() {
    setUpModal("Problème", "", "");
    getLog('getFromID response error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données");
    updateModal(false, html, '');
  };
  request.send();
}
function setDept(){
 setGeneric(
 'departements');
}
function setBrand(){
 setGeneric(
 'enseignes');
}
function setCategory(){
 setGeneric(
 'categories');
}
function setLocation(){
 setGeneric(
 'emplacements');
}
function setStatus(){
 setGeneric(
 'statuts');
}
function setGeneric(route){
  getLog('setGeneric routeNom', document.getElementById(route + "Nom").value)
  var json = JSON.stringify({
     id: document.getElementById(route + "ID").value,
     code: document.getElementById(route + "Code").value,
     nom: document.getElementById(route + "Nom").value,
   });
  setUpModal("Enregistrement en cours", "Enregistrement en cours ...", "");
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('PUT', getServerURL() + "/" + route, true);
  request.setRequestHeader('Authentication-Token', token);
  request.setRequestHeader('content-type', 'application/json');
  funcOnSuccess = function(resp){
    searchItemsOnKeypress();
  }
  request.onload = function() {
    callbackOnAPI('setGeneric', this.status, this.response, funcOnSuccess);
  }
  request.onerror = function() {
    getLog('setGeneric response error', this.response);
    html = respToHTML(this.response, "Erreur lors de l'enregistrement des données");
    updateModal(false, html, '');
  }
  request.send(json);
}
function getList(path) {
  getLog("getList in", path);
  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/" + path, true);
  request.setRequestHeader('Authentication-Token', token);

  request.onload = function() {
    /*TODO Use callbackOnAPI and make it work with a modo user*/
    if (this.status >= 200 && this.status < 400) {
      var resp = JSON.parse(this.response);
      var list_html = "";
      var list_url = path + 'List';
      var list_content = document.getElementById(list_url);
      var type = resp["response"]["data"][0]["type"]
      resp["response"]["data"].forEach(function(item, index){
        var button = type + "NomItem";
        list_html = list_html + "<li but='"+ button +"' onclick='listClick(this)' value='" + item["id"] + "' class='dropdown-item'>" + item["nom"] + "</li>";
      });
      list_content.innerHTML = list_html;
    }
    else{
      html = respToHTML(this.response, "Erreur lors de la récupération des données.");
      updateModal(false, html, '');
    }
  }
  request.onerror = function() {
    getLog('getList error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données.");
    updateModal(false, html, '');
  }
  request.send();
}
function exportItemsCsv(){
  setUpModal("Export du fichier", '', '');

  var token = getCookie("user_token");
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items", true);
  request.setRequestHeader('Authentication-Token', token)
  request.setRequestHeader('accept', 'text/csv')
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      var file = new Blob([this.response], {type: 'text/csv'});
      document.getElementById("messageInfoLink").setAttribute("href", URL.createObjectURL(file));
      document.getElementById("messageInfoLink").setAttribute("download", "export.csv");
      document.getElementById("messageInfoLink").innerHTML = "Cliquer ici pour le télécharger";
    }
    else {
      document.getElementById("messageInfo").innerHTML = "Erreur lors de la création du fichier";
    }
  }
  request.onerror = function() {
    getLog('exportItemsCsv error', this.response);
    html = respToHTML(this.response, "Erreur lors de la récupération des données.");
    updateModal(false, html, '');
  }
  request.send();
}
function confirmToken(token) {
  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/auth/confirm/" + token, true);
  funcOnSuccess = function(resp){
    document.getElementById("confirmText").innerHTML = "Compte confirmé.";
    window.history.pushState({page: 'confirm'}, 'Détail', '/index.html');
  };
  funcOnError = function(resp){
    document.getElementById("confirmText").innerHTML = "Le lien est invalide ou expiré.";
    window.history.pushState({page: 'confirm'}, 'Détail', '/index.html');
  }
  request.onload = function() {
    callbackOnAPI('confirmToken', this.status, this.response, funcOnSuccess, funcOnError);
  }
  request.onerror = function() {
    getLog('confirmToken error', this.response);
    html = respToHTML(this.response, "Erreur lors de la confirmation.");
    updateModal(false, html, '');
  }
  request.send();
}

// Items
function searchItemsClean(){
  document.getElementById(
  "tableBody").innerHTML = '';
}
function searchItemsOnKeypress(){
  var searchText = document.getElementById("searchText").value;
  getLog("searchItemsOnKeypress: ", searchText);
  clearTimeout(timerId);
  var len = searchText.length;
  if (len < 3)
  {
    searchItemsClean();
  }
  else {
    document.getElementById("tableBody").textContent = "Chargement";
    timerId = setTimeout(searchItemsTable, 1000);
  }
}
function searchItemsTable() {
  var searchText = document.getElementById("searchText").value;
  getLog("searchItemsTable: ", searchText);
  var token = getCookie("user_token");

  var request = new XMLHttpRequest();
  request.open('GET', getServerURL() + "/items?recherche="+searchText, true);
  request.setRequestHeader('Authentication-Token', token)
  funcOnSuccess = function(resp){
      this.html = '';
      document.getElementById("tableBody").innerHTML = '';
      this.html = this.html + "<table class='table table-bordered'>";
      this.html = this.html + "<thead class='thead'>";
      this.html = this.html + "<tr>";
      this.html = this.html + "<th scope='col'>Département</th>";
      this.html = this.html + "<th scope='col'>Ville</th>";
      this.html = this.html + "<th scope='col'>Catégorie</th>";
      this.html = this.html + "<th scope='col'>Enseigne</th>";
      this.html = this.html + "<th scope='col'></th>";
      this.html = this.html + "</tr>";
      this.html = this.html + "</thead>";
      this.html = this.html + "<tbody>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = this.html;
      getLog("searchItemsTable this.response", resp);
      resp["response"]["data"].forEach(function(item, index){
        rank = index + 1;
        this.html = this.html + "<tr>";
        this.html = this.html + "<td>" + item["departement_code"] + " - "+ item["departement_name"] + "</td>";
        this.html = this.html + "<td>" + item["location_city"] + "</td>";
        this.html = this.html + "<td>" + item["category_name"] + "</td>";
        this.html = this.html + "<td>" + item["brand_name"] + "</td>";
        this.html = this.html + "<td><a class='kinda-link text-primary' onclick='loadItemPage("+ item["id"] +")'>" + "<i class='icons-search' aria-hidden='true'></i>" + "</a></td>";
        this.html = this.html + "</tr>";
      });
      this.html = this.html + "</tbody>";
      this.html = this.html + "</table>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = this.html;
  };
  funcOnError = function(resp){
      this.html = '';
      this.html = this.html + "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
      this.html = this.html + "<h6 class='h6 font-weight-normal text-center text-danger' >Problème de connexion au serveur</h6>";
      this.html = this.html + "</div>";
      document.getElementById("tableBody").innerHTML = '';
      document.getElementById("tableBody").innerHTML = html;
  }
  request.onload = function() {
    callbackOnAPI('searchItemsTable', this.status, this.response, funcOnSuccess, funcOnError, true);
  };
  request.onerror = function() {
    var html = '';
    html = html + "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
    html = html + "<h6 class='h6 font-weight-normal text-center text-danger' >Problème de traitement de la page</h6>";
    html = html + "</div>";
    document.getElementById("tableBody").innerHTML = '';
    document.getElementById("tableBody").innerHTML = html;
  };
  request.send();
}
