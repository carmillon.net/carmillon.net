#    config.py
#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # sqlalchemy configs: http://flask-sqlalchemy.pocoo.org/2.1/config/#configuration-keys
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' \
                              + os.path.join(basedir, 'app.db')

    # flask-cors configuration
    CORS_ENABLE = True

    # Celery
    CELERY_ENABLE = False
    CELERY_BROKER_URL = 'redis://localhost:6379'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'


class DevConfig(Config):
    CONFIG_TYPE = 'DEV'
    SECRET_KEY = 'dev'
    SECURITY_PASSWORD_SALT = 'dev_salt'
    SQLALCHEMY_ECHO = False
    DEBUG = True

    # flask-mail
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    # mail authentication
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    # mail accounts
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')


class TestConfig(Config):
    CONFIG_TYPE = 'TEST'
    SECRET_KEY = 'test'
    SECURITY_PASSWORD_SALT = 'dev_test'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///'
    DEBUG = True
    TESTING = True


class ProdConfig(Config):
    CONFIG_TYPE = 'PROD'
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_POOL_RECYCLE = 280

    # Celery
    CELERY_ENABLE = True
    CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL')
    CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND')

    # flask-mail
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    # mail authentication
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    # mail accounts
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')
