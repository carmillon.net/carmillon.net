#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail
from sqlalchemy import MetaData
from celery import Celery

metadata = MetaData(
    naming_convention={
        'pk': 'pk_%(table_name)s',
        'fk': 'fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s',
        'ix': 'ix_%(table_name)s_%(column_0_name)s',
        'uq': 'uq_%(table_name)s_%(column_0_name)s',
        # Fail with sqlite
        # 'ck': 'ck_%(table_name)s_%(constraint_name)s',
    }
)

# TODO https://github.com/pallets/flask-sqlalchemy/issues/589#issuecomment-361075700


db = SQLAlchemy(metadata=metadata)

migrate = Migrate()
cors = CORS()
login_manager = LoginManager()
mail = Mail()
celery = Celery(__name__, broker='redis://localhost:6379')
