#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Roles and users (Database object)."""

from carmillon.extensions import db
from datetime import datetime


class UserByRoleDB(db.Model):
    """
    Database object for the relation between Roles and Users.

    Columns:
        id = Internal code, only used for the integrity of the database.

        users_id = User ID.
        roles_id = Role ID.
    """
    __tablename__ = 'users_by_roles'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def add(self, user, role, user_id=1):
        self.created_by_id = user_id
        self.created_time = datetime.utcnow()
        self.user_id = user
        self.role_id = role
        db.session.add(self)
        self.set_modified(user_id)

    def remove(self):
        """
        Delete an item.

        Return values (boolean):
        - False: Not deleted, doesnt exist
        - True: Deleted successfully
        """
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_modified(self, user_id):
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        db.session.commit()
