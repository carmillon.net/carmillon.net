#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Locations and operations on them (Database object)."""

from carmillon.extensions import db
from carmillon.geocoding import Geocoding

from datetime import datetime


class LocationDB(db.Model):
    """
    Database object for the status.

    Columns:
        id = Internal code, only used for the integrity of the database.

    """
    __tablename__ = 'locations'

    id = db.Column(db.Integer, primary_key=True)
    code_text = db.Column(db.String)
    name_text = db.Column(db.String)
    postcode_text = db.Column(db.String)
    gare_bool = db.Column(db.Boolean)
    lat_float = db.Column(db.Float)
    lon_float = db.Column(db.Float)
    dept_code_text = db.Column(db.String)
    dept_name_text = db.Column(db.String)

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def add(self, user_id=1):
        if self.id is None:
            self.created_by_id = user_id
            self.created_time = datetime.utcnow()
            db.session.add(self)
            self.set_modified(user_id)
            return True
        return False

    def delete(self):
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_name(self, name, user_id):
        self.name_text = name
        self.set_modified(user_id)

    def get_name(self):
        return self.name_text

    def set_code(self, code, user_id):
        self.code_text = code
        self.set_modified(user_id)

    def set_modified(self, user_id):
        self.set_postcode(user_id)
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        db.session.commit()

    def set_postcode(self, user_id):
        if self.gare_bool is True and self.gare_bool is not None:
            geocode = Geocoding().gare(self.name_text)
            if geocode is not None:
                self.postcode_text = geocode['code_postal']
                self.lon_float = geocode['lon']
                self.lat_float = geocode['lat']
                self.dept_code_text = geocode['context'][0]
                self.dept_name_text = geocode['context'][1]
                return True
            else:
                return False
        geocode = Geocoding().commune(self.name_text)
        if geocode is None:
            return False
        self.postcode_text = geocode['code_postal']
        return True

    def get_lat(self):
        return self.lat_float

    def get_lon(self):
        return self.lon_float

    def get_dept_code(self):
        return self.dept_code_text

    def get_dept_name(self):
        return self.dept_name_text

    def set_gare(self, gare, user_id):
        self.gare_bool = gare
        self.set_modified(user_id)

    def is_gare(self):
        if self.gare_bool is True and self.gare_bool is not None:
            return True
        return False
