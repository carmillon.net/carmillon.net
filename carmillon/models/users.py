#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Users and operations on them (Database object)."""

from werkzeug.security import generate_password_hash, check_password_hash
from carmillon.extensions import db
from datetime import datetime
from random import *
import string


class UserDB(db.Model):
    """
    Database object for the users.

    Columns:
        id = Internal code, only used for the integrity of the database.

        email = Email address and login for user.
        password_hash = Password stored as a hash.

        username_text = Username, and name displayed on the application
        (Unique).
    """
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(255), nullable=False)
    password_hash = db.Column(db.String(255))
    username_text = db.Column(db.String(255))
    registered_on = db.Column('registered_on', db.DateTime)
    active_bool = db.Column(db.Boolean)
    confirmed = db.Column(db.Boolean, default=False)
    confirmed_on = db.Column(db.DateTime)

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def __repr__(self):
        return '<User %r>' % self.email

    def add(self, password, restricted=True, user_id=1):
        """
        Add a user.

        Return values (string):
        - OK, or the error message
        """
        empty_list = (None, "", False, " ")
        sncf_list = ("sncf.fr", "sncf.com", "reseau.sncf.fr", "lavergne.online")
        if self.email in empty_list:
            return False, "Erreur, le mail est vide"
        email_control = self.email.split("@")
        if len(email_control) != 2:
            return False, "Erreur, le mail a plusieurs @"
        if password in empty_list:
            return False, "Erreur, le mot de passe est vide"
        if restricted is True:
            if email_control[1] not in sncf_list:
                return False, "Erreur, seules les adresses sncf.fr sont acceptées"
            if email_control[0].split(".")[0] == "ext":
                return False, "Erreur, les addresses mail externes ne sont pas acceptées"
        if restricted is True:
            self.confirmed = False
        else:
            self.confirmed = True
        self.registered_on = datetime.utcnow()
        self.active_bool = True
        self.created_by_id = user_id
        self.created_time = datetime.utcnow()
        db.session.add(self)
        self.set_password(password)
        self.set_modified(user_id)
        return True, "OK"

    def delete(self):
        """
        Delete a user.

        Return values (boolean):
        - False: Not deleted, doesnt exist
        - True: Deleted successfully
        """
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_password(self, password, user_id=1):
        """
        Set the password for self user.

        Return values (boolean):
        - False: Not set, doesnt exist
        - True: Setting successfully
        """
        self.password_hash = generate_password_hash(password)
        self.set_modified(user_id)
        return True

    def reset_password(self):
        min_char = 8
        max_char = 12
        allchar = string.ascii_letters + string.punctuation + string.digits
        password = "".join(choice(allchar) for x in range(randint(min_char, max_char)))
        self.set_password(password)
        return password

    def set_confirmed(self, user_id):
        self.confirmed = True
        self.confirmed_on = datetime.now()
        self.set_modified(user_id)

    def get_email(self):
        """
        Get the email of self user.

        Return values (string):
        - The email of the user
        """
        if self.id is not None:
            return self.email
        return False

    def get_code(self):
        """
        Get the code of self user (by default, the email).

        Return values (string):
        - The code of the user (email)
        """
        return self.get_email()

    def get_username(self):
        """
        Get the username of self user.

        Return values (string):
        - The username of the user
        """
        if self.id is not None:
            if self.username_text is None or self.username_text == "":
                return self.email.split("@")[0]
            return self.username_text
        return False

    def set_username(self, username):
        """
        Set the username of self user.

        Return values (dict):
        - ["statut"]: OK or KO
        - ["message"]: Information of the set operation
        """
        return_dict = {}
        query = db.session.query(UserDB)\
            .filter(UserDB.username_text == username)\
            .first()
        if query is None:
            self.username_text = username
            db.session.commit()
            return_dict["statut"] = "OK"
            return_dict["message"] = "Username set OK"
        else:
            return_dict["statut"] = "KO"
            return_dict["message"] = "Username already used"
        return return_dict

    def check_password(self, password):
        """
        Check if a password is equal to the user password.

        Arguments:
        - password: string to check against the user password

        Return values (boolean):
        - True: Password matched
        - False: Password didnt match.
        """
        return check_password_hash(self.password_hash, password)

    def is_active(self):
        """Return active status"""
        return self.active_bool

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    @staticmethod
    def is_authenticated():
        """Return True if the user is authenticated."""
        return True

    @staticmethod
    def is_anonymous():
        """False, as anonymous users aren't supported."""
        return False

    def set_modified(self, user_id):
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        db.session.commit()
