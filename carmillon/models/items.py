#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Items and operations on them (Database object)."""

from flask import current_app

from carmillon.extensions import db
from carmillon.models.status import StatusDB
from carmillon.models.dept import DeptDB
from carmillon.models.brands import BrandDB
from carmillon.models.locations import LocationDB
from carmillon.models.categories import CategoryDB
from carmillon.geocoding import Geocoding

from datetime import datetime
import time


class ItemDB(db.Model):
    """
    Database object for the items.

    Columns:
        id = Internal code, only used for the integrity of the database.

        name_text = Name displayed on the application.
    """
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    description_text = db.Column(db.String)
    address_text = db.Column(db.String)
    search_text = db.Column(db.String)
    status_date = db.Column(db.DateTime)
    source_text = db.Column(db.String)

    address_full_text = db.Column(db.String)
    lat_float = db.Column(db.Float)
    lon_float = db.Column(db.Float)
    dept_code_text = db.Column(db.String)
    dept_name_text = db.Column(db.String)
    city_name_text = db.Column(db.String)

    status_id = db.Column(db.Integer, db.ForeignKey('status.id'))
    status = db.relationship("StatusDB", foreign_keys=[status_id])
    dept_id = db.Column(db.Integer, db.ForeignKey('dept.id'))
    dept = db.relationship("DeptDB", foreign_keys=[dept_id])
    brand_id = db.Column(db.Integer, db.ForeignKey('brands.id'))
    brand = db.relationship("BrandDB", foreign_keys=[brand_id])
    location_id = db.Column(db.Integer, db.ForeignKey('locations.id'))
    location = db.relationship("LocationDB", foreign_keys=[location_id])
    category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    category = db.relationship("CategoryDB", foreign_keys=[category_id])

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def add(self, status_id=None, status_code=None, dept_id=None, brand_id=None, location_id=None, address=None, category_id=None,
            source_text=None, update=True, user_id=1):
        if self.id is None:
            if status_id is not None:
                self.status_id = status_id
            if status_code is not None:
                status = StatusDB.query.filter_by(code_text=status_code).first()
                if status is not None:
                    self.status_id = status.id
            if dept_id is not None:
                self.dept_id = dept_id
            if brand_id is not None:
                self.brand_id = brand_id
            if location_id is not None:
                self.location_id = location_id
            if address is not None:
                self.address_text = address
            if category_id is not None:
                self.category_id = category_id
            if source_text is not None:
                self.source_text = source_text
            self.created_by_id = user_id
            self.created_time = datetime.utcnow()
            db.session.add(self)
            db.session.commit()
            if update is True:
                self.update(user_id)
                self.update_geo(user_id)
            return True
        return False

    def delete(self):
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_search_text_list(self):
        query = db.session.query(StatusDB.name_text,
                                 DeptDB.name_text,
                                 DeptDB.code_text,
                                 BrandDB.name_text,
                                 LocationDB.name_text,
                                 CategoryDB.name_text) \
            .outerjoin(ItemDB.status) \
            .outerjoin(ItemDB.dept) \
            .outerjoin(ItemDB.brand) \
            .outerjoin(ItemDB.location) \
            .outerjoin(ItemDB.category) \
            .filter(ItemDB.id == self.id)

        result = query.first()
        set_string = ""
        # current_app.logger.info('debug result: %s', result)
        if result is not None:
            for val in result:
                set_string = set_string + str(val).lower() + str("|")
            self.search_text = set_string

        list_to_add = [self.dept_code_text,
                       self.dept_name_text,
                       self.city_name_text]
        for i in list_to_add:
            if i is not None:
                self.search_text = self.search_text + str(i).lower() + str("|")

    def set_modified(self, user_id):
        self.set_search_text_list()
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        db.session.commit()

    def set_description(self, description, user_id):
        if description is not None:
            self.description_text = description
            self.set_modified(user_id)
            return True
        return False

    def get_description(self):
        return self.description_text

    def set_address(self, address, user_id):
        if address is not None:
            self.address_text = address
            self.set_modified(user_id)
            return True
        return False

    def get_address(self):
        if self.address_full_text is None or self.address_full_text == "":
            return self.address_text
        return self.address_full_text

    def set_status(self, user_id=1, status_id=None, status_name=None, status_code=None):
        if status_id is not None and status_name is None and status_code is None:
            self.status_id = status_id
            self.status_date = datetime.utcnow()
            self.set_modified(user_id)
            return True
        if status_id is None and status_name is not None and status_code is None:
            status = StatusDB.query.filter_by(name_text=status_name).first()
            if status is not None:
                self.status_id = status.id
                self.status_date = datetime.utcnow()
                self.set_modified(user_id)
                return True
            return False
        if status_id is None and status_name is None and status_code is not None:
            status = StatusDB.query.filter_by(code_text=status_code).first()
            if status is not None:
                self.status_id = status.id
                self.status_date = datetime.utcnow()
                self.set_modified(user_id)
                return True
            return False
        return False

    def get_status(self):
        return StatusDB.query.filter_by(id=self.status_id).first().get_name()

    def get_status_code(self):
        return StatusDB.query.filter_by(id=self.status_id).first().get_code()

    def set_category(self, user_id, category_id=None, category_name=None):
        if category_id is not None and category_name is None:
            self.category_id = category_id
            self.set_modified(user_id)
            return True
        if category_id is None and category_name is not None:
            category = CategoryDB.query.filter_by(name_text=category_name).first()
            if category is not None:
                self.category_id = category.id
                self.set_modified(user_id)
                return True
            return False
        return False

    def set_dept(self, user_id, dept_id=None, dept_name=None):
        if dept_id is not None and dept_name is None:
            self.dept_id = dept_id
            self.set_modified(user_id)
            return True
        if dept_id is None and dept_name is not None:
            dept = DeptDB.query.filter_by(name_text=dept_name).first()
            if dept is not None:
                self.dept_id = dept.id
                self.set_modified(user_id)
                return True
            return False
        return False

    def set_brand(self, user_id, brand_id=None, brand_name=None):
        if brand_id is not None and brand_name is None:
            self.brand_id = brand_id
            self.set_modified(user_id)
            return True
        if brand_id is None and brand_name is not None:
            brand = BrandDB.query.filter_by(name_text=brand_name).first()
            if brand is not None:
                self.brand_id = brand.id
                self.set_modified(user_id)
                return True
            return False
        return False

    def set_location(self, user_id, location_id=None, location_name=None):
        if location_id is not None and location_name is None:
            self.location_id = location_id
            self.set_modified(user_id)
            return True
        if location_id is None and location_name is not None:
            location = LocationDB.query.filter_by(name_text=location_name).first()
            if location is not None:
                self.location_id = location.id
                self.set_modified(user_id)
                return True
            return False
        return False

    def set_address_full_text(self, address=None, user_id=1):
        if address is not None:
            self.address_full_text = address
            self.set_modified(user_id)
            return True
        if self.location_id is not None:
            location = LocationDB().query.filter_by(id=self.location_id).first()
            if location is None:
                return False
            if location.postcode_text is None:
                return False
            if self.address_full_text == self.address_text:
                return False
            self.address_full_text = self.address_text + " " + location.postcode_text + " " + location.name_text
            self.address_text = self.address_full_text
            self.set_modified(user_id)
            return True
        if self.address_full_text == self.address_text:
            self.address_full_text = self.address_text + ", " + self.city_name_text
            self.set_modified(user_id)
        if self.address_full_text is None:
            self.address_full_text = self.address_text
            self.set_modified(user_id)
            return True
        return True

    def set_coord(self, lat=None, lon=None, user_id=1):
        if lat is None and lon is None:
            return False
        if lat is not None:
            self.lat_float = lat
        if lon is not None:
            self.lon_float = lon
        self.set_modified(user_id)
        return True

    def get_coord(self):
        return {'lat': self.lat_float, 'lon': self.lon_float}

    def get_dept_code(self):
        if self.dept_code_text is not None:
            return self.dept_code_text
        return None

    def get_dept_name(self):
        if self.dept_name_text is not None:
            return self.dept_name_text
        return None

    def get_location_name(self):
        if self.city_name_text is not None:
            return self.city_name_text
        return None

    def update_geo(self, user_id):
        if self.address_full_text is not None:
            location = LocationDB.query.filter_by(id=self.location_id).first()
            if location is not None:
                if location.is_gare():
                    self.set_coord(lat=location.get_lat(), lon=location.get_lon())
                    self.dept_code_text = location.get_dept_code()
                    self.dept_name_text = location.get_dept_name()
                    self.city_name_text = location.get_name()
                    self.set_modified(user_id)
                    time.sleep(1)
                    return True
            geo = Geocoding().adresse(self.get_address())
            if geo is not None:
                self.set_coord(lat=geo['lat'], lon=geo['lon'])
                self.dept_code_text = geo['context'][0]
                self.dept_name_text = geo['context'][1]
                if geo['city'] in ["Paris"]:
                    self.city_name_text = geo['district']
                else:
                    self.city_name_text = geo['city']
                location = LocationDB.query.filter_by(id=self.location_id).first()
                if location is None:
                    self.location_id = None
                    self.dept_id = None
                    self.set_modified(user_id)
                    time.sleep(1)
                    return True
                elif location.is_gare() is False:
                    if location.postcode_text is not None:
                        self.location_id = None
                        self.dept_id = None
                        self.set_modified(user_id)
                        time.sleep(1)
                        return True
            time.sleep(1)

    def format(self):
        status_date = None
        if self.status_date is not None:
            status_date = self.status_date.strftime("%d/%m/%Y")
        return_dict = {'id': self.id,
                       'description_text': self.description_text,
                       'address_text': self.get_address(),
                       'status_date': status_date,
                       'dept_code_text': self.get_dept_code(),
                       'dept_name_text': self.get_dept_name(),
                       'locations_name_text': self.get_location_name(),
                       'status_name_text': None,
                       'categories_name_text': None,
                       'brands_name_text': None,
                       'lat_float': self.lat_float,
                       'lon_float': self.lon_float,
                       'category_id': self.category_id,
                       'brand_id': self.brand_id,
                       'dept_id': self.dept_id,
                       'locations_id': self.location_id,
                       'status_id': self.status_id,
                       'created_by_id': self.created_by_id
                       }
        return return_dict

    def update(self, user_id=1):
        self.set_address_full_text(address=None, user_id=user_id)
        self.update_geo(user_id)
        self.set_modified(user_id)
