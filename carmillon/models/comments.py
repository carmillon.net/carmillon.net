#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Comments and operations on them (Database object)."""

from carmillon.extensions import db
from carmillon.models.users import UserDB

from flask import current_app

from datetime import datetime


class CommentsDB(db.Model):
    """
    Database object for the comments.

    Columns:
        id = Internal code, only used for the integrity of the database.

    """
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    message_text = db.Column(db.String)
    validation_bool = db.Column(db.Boolean, default=False)
    validation_date = db.Column(db.DateTime)
    active_bool = db.Column(db.Boolean, default=True)
    
    # report
    report_bool = db.Column(db.Boolean, default=False)
    report_by_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    report_time = db.Column(db.DateTime)

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def add(self, message=None, validation=None, date=None, user_id=1):
        if message is None and validation is None:
            return False
        if self.id is None:
            self.created_time = datetime.utcnow()
            self.created_by_id = user_id
            if message is not None:
                self.message_text = message
            if validation is not None:
                self.validation_bool = validation
                if date is not None:
                    self.validation_date = date
                else:
                    self.validation_date = datetime.utcnow()
            db.session.add(self)
            self.set_modified(user_id)
            return True
        return False

    def delete(self):
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_modified(self, user_id):
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        print(self)
        db.session.commit()

    def get_message(self):
        return self.message_text

    def set_message(self, message, user_id=1):
        self.message_text = message
        self.set_modified(user_id)
    
    def get_validation(self):
        return self.validation_bool

    def set_validation(self, validation, date=None, user_id=1):
        self.validation_bool = validation
        if date is not None:
            self.validation_date = date
        else:
            self.validation_date = datetime.utcnow()
        self.set_modified(user_id)

    def get_active(self):
        return self.active_bool

    def set_active(self, active, user_id=1):
        self.active_bool = active
        self.set_modified(user_id)
        
    def get_report(self):
        return {"report": self.report_bool,
                "report_by_id": self.report_by_id,
                "report_time": self.report_time}
    
    def set_report(self, report, report_by, report_time=datetime.utcnow()):
        if not isinstance(report, bool):
            return False
        user = UserDB.query.filter_by(id=report_by).first()
        if user is None:
            return False
        if not isinstance(report_time, datetime):
            return False
        if report is True:
            self.report_bool = report
            self.report_by_id = report_by
            self.report_time = report_time
            return True
        self.report_bool = False
        self.report_by_id = None
        self.report_time = None
        return True

    def get_modified_username(self):
        user = UserDB.query.filter_by(id=self.modified_by_id).first()
        if user is None:
            return None
        return UserDB.query.filter_by(id=self.modified_by_id).first().get_username()

    def get_created_username(self):
        user = UserDB.query.filter_by(id=self.created_by_id).first()
        if user is None:
            return None
        return UserDB.query.filter_by(id=self.created_by_id).first().get_username()

    def get_report_username(self):
        user = UserDB.query.filter_by(id=self.report_by_id).first()
        if user is None:
            return None
        return UserDB.query.filter_by(id=self.report_by_id).first().get_username()

    def format(self):
        return {'id': self.id,
                'message_text': self.message_text,
                'validation_bool': self.validation_bool,
                'active_bool': self.active_bool,
                'created_by_id': self.created_by_id,
                'created_by_username': self.get_created_username(),
                'created_time': self.created_time,
                'modified_time': self.modified_time,
                'modified_by_id': self.modified_by_id,
                'modified_by_username': self.get_modified_username(),
                'report_by_username': self.get_report_username(),
                'report_bool': self.report_bool,
                'report_time': self.report_time,
                }
