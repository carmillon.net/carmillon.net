#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Status and operations on them (Database object)."""

from carmillon.extensions import db

from datetime import datetime


class StatusDB(db.Model):
    """
    Database object for the status.

    Columns:
        id = Internal code, only used for the integrity of the database.

    """
    __tablename__ = 'status'

    id = db.Column(db.Integer, primary_key=True)
    code_text = db.Column(db.String)
    name_text = db.Column(db.String)

    # information
    created_time = db.Column(db.DateTime, default=datetime.utcnow)
    created_by_id = db.Column(db.Integer, default=1)
    modified_time = db.Column(db.DateTime, default=datetime.utcnow)
    modified_by_id = db.Column(db.Integer, default=1)

    def add(self, user_id=1):
        if self.id is None:
            self.created_by_id = user_id
            self.created_time = datetime.utcnow()
            db.session.add(self)
            self.set_modified(user_id)
            return True
        return False

    def delete(self):
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()
            return True
        return False

    def set_modified(self, user_id):
        self.modified_by_id = user_id
        self.modified_time = datetime.utcnow()
        db.session.commit()

    def set_name(self, name, user_id):
        self.name_text = name
        self.set_modified(user_id)

    def set_code(self, code, user_id):
        self.code_text = code
        self.set_modified(user_id)

    def get_name(self):
        return self.name_text

    def get_code(self):
        return self.code_text
