#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Resource, Namespace
from flask import current_app
from carmillon.api.common import message, auth_token_needed, roles_required
from carmillon.extensions import db, celery
from carmillon.models.users import UserDB
from carmillon.models.roles import RoleDB
from carmillon.models.users_by_roles import UserByRoleDB
from carmillon.models.comments import CommentsDB
from carmillon.models.items import ItemDB
from carmillon.models.status import StatusDB

from os import environ
from datetime import datetime

import json

api = Namespace('DB', description='Operations on the database')


@api.route('/init')
class DbInitAPI(Resource):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Init the database")
    def post(self):
        return_message = {'success': False, 'code_app': 'C1100', 'text': ''}
        user = None
        try:
            user = UserDB.query.first()
        except:
            db.create_all()
            return_message['text'] = "Base initialisée. "
            user = UserDB.query.first()
        if user is None:
            if environ.get('CARMILLON_ADMIN_EMAIL') and environ.get('CARMILLON_ADMIN_PASSWORD'):
                UserDB(email=environ.get('CARMILLON_ADMIN_EMAIL'))\
                    .add(password=environ.get('CARMILLON_ADMIN_PASSWORD'), restricted=False)
            else:
                UserDB(email='admin@admin.com').add(password='admin', restricted=False)
        role = RoleDB.query.first()
        if role is None:
            RoleDB().add(code='admin')
            return_message['text'] = return_message['text'] + 'Role admin créé. '
        role_user = UserByRoleDB.query.first()
        if role_user is None:
            UserByRoleDB().add(1, 1)
            return_message['text'] = return_message['text'] + 'Ajout du role admin au User admin. '

        role_modo = RoleDB().query.filter_by(code_text='modo').first()
        if role_modo is None:
            RoleDB().add(code='modo')
            return_message['text'] = return_message['text'] + 'Role modo créé. '

        users_admin = UserByRoleDB().query.all()

        for i in users_admin:
            user = UserDB.query.filter_by(id=i.user_id).first()
            if user.confirmed is False or user.confirmed is None:
                user.confirmed = True
                user.confirmed_on = datetime.now()
                user.set_modified(1)
                return_message['text'] = return_message['text'] + 'Utilisateur ' + user.email + ' confirmed.'

        status_confirmed = StatusDB.query.filter_by(code_text="CONFIRME-01").first()
        if status_confirmed is None:
            StatusDB(name_text="Confirmé", code_text="CONFIRME-01").add()
            return_message['text'] = return_message['text'] + 'Statut Confirmé créé.'
            if current_app.config['CELERY_ENABLE'] is True:
                update_items.delay(1)
                return_message['text'] = return_message['text'] + 'Migration des statuts lancé.'
            else:
                update_items(1)
                return_message['text'] = return_message['text'] + 'Migration des statuts faite.'
        status_proposed = StatusDB.query.filter_by(code_text="PROPOSE-01").first()
        if status_proposed is None:
            StatusDB(name_text="Proposé", code_text="PROPOSE-01").add()
            return_message['text'] = return_message['text'] + 'Statut Proposé créé.'
        status_invalid = StatusDB.query.filter_by(code_text="INVALIDE-01").first()
        if status_invalid is None:
            StatusDB(name_text="Invalide", code_text="INVALIDE-01").add()
            return_message['text'] = return_message['text'] + 'Statut Invalide créé.'
        return_message['success'] = True
        return_message['code_app'] = 'C1101'
        return_message['text'] = return_message['text'] + 'Init OK. '
        return return_message, 200


@celery.task
def update_items(user_id):
    items = ItemDB.query.all()
    for i in items:
        i.set_status(user_id=user_id, status_code="CONFIRME-01")
    return True


@api.route('/infos')
class DbInfos(Resource):
    @api.response(200, 'OK')
    @api.doc("Infos sur la base")
    @api.doc(security='apikey')
    @auth_token_needed
    @roles_required('admin')
    def get(self):
        users = db.session.query(db.func.count(UserDB.id)).scalar()
        comments = db.session.query(db.func.count(CommentsDB.id)).scalar()
        items = db.session.query(db.func.count(ItemDB.id)).scalar()
        infos = {
                 'CONFIG_TYPE': current_app.config['CONFIG_TYPE'],
                 'CORS_ENABLE': current_app.config['CORS_ENABLE'],
                 'CELERY_ENABLE': current_app.config['CELERY_ENABLE'],
                 'CELERY_BROKER_URL': current_app.config['CELERY_BROKER_URL'],
                 'CELERY_RESULT_BACKEND': current_app.config['CELERY_RESULT_BACKEND'],
                 'Utilisateurs': users,
                 'Commentaires': comments,
                 'Avantages': items,
                 }

        return infos
