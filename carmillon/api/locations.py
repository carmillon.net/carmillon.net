#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Resource, Namespace
from flask import current_app
from carmillon.api.common import location_parser, location_list_message, auth_token_needed, roles_required,\
    get_id_from_token, message
from carmillon.models.locations import LocationDB
from carmillon.models.items import ItemDB
from carmillon.extensions import db, celery

import time

api = Namespace('Ville / Gare', description='Operations sur les villes ou gares')


@celery.task
def update_all_locations(user_id):
    locations = LocationDB().query.all()
    for i in locations:
        i.set_modified(user_id)
        time.sleep(1)


@api.route('/')
@api.route('')
class LocationAPI(Resource):
    @api.response(200, 'OK')
    @api.response(404, 'Not found')
    @api.doc("Get the locations")
    @api.doc(security='apikey')
    @api.marshal_with(location_list_message, envelope='response')
    @auth_token_needed
    @api.expect(location_parser, validate=True)
    def get(self):
        return_message = {'success': False, 'code_app': 'C2000', 'text': None, 'data': []}
        args = location_parser.parse_args()
        if args['id'] is not None:
            result = LocationDB().query.filter_by(id=args['id']).all()
        elif args['code'] is not None:
            result = LocationDB().query.filter_by(code_text=args['code']).all()
        elif args['nom'] is not None:
            result = LocationDB().query.filter_by(name_text=args['nom']).all()
        else:
            result = db.session.query(LocationDB)\
                .join(ItemDB)\
                .order_by("name_text").all()
        for i in result:
            return_message['data'].append(i)
        return_message['success'] = True
        return_message['code_app'] = 'C2001'
        return_message['text'] = 'Search OK'
        return return_message, 200

    @api.response(201, 'Create')
    @api.response(404, 'Not found')
    @api.marshal_with(location_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a location")
    @roles_required('modo')
    @api.expect(location_parser, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C2100', 'text': None}
        args = location_parser.parse_args()
        if args['code'] is None or 'code' not in args:
            if args['nom'] is None or 'nom' not in args:
                return_message['code_app'] = 'C2101'
                return_message['text'] = 'Error, you need to specify a code or a name'
                return return_message, 401
            LocationDB(name_text=args['nom'], code_text=args['nom']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2102'
            return_message['text'] = 'Adding OK (code missing)'
            return return_message, 201
        if args['nom'] is None or 'nom' not in args:
            LocationDB(name_text=args['code'], code_text=args['code']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2103'
            return_message['text'] = 'Adding OK (name missing)'
            return return_message, 201
        LocationDB(name_text=args['nom'], code_text=args['code']).add()
        return_message['success'] = True
        return_message['code_app'] = 'C2104'
        return_message['text'] = 'Adding OK'
        return return_message, 201

    @api.response(200, 'OK')
    @api.response(204, 'OK')
    @auth_token_needed
    @roles_required('admin')
    @api.doc(security='apikey')
    @api.doc("Delete a location")
    @api.expect(location_parser, validate=True)
    @api.marshal_with(location_list_message, envelope='response')
    def delete(self):
        return_message = {'success': False, 'code_app': 'C2600', 'text': None}
        args = location_parser.parse_args()
        if args['id'] is None:
            return_message['text'] = 'Un ID doit être spécifié.'
            return_message['code_app'] = 'C2601'
            return return_message, 401
        location = LocationDB().query.filter_by(id=args['id']).first()
        if location is None:
            return_message['text'] = 'L\'emplacement n\'existe pas.'
            return_message['code_app'] = 'C2602'
            return return_message, 404
        location.delete()
        return_message['success'] = True
        return_message['code_app'] = 'C2603'
        return_message['text'] = 'Suppression effectuée'
        return return_message, 200

    @api.response(201, 'Modified')
    @api.response(404, 'Not found')
    @api.marshal_with(location_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Update a location")
    @roles_required('modo')
    @api.expect(location_parser, validate=True)
    def put(self):
        return_message = {'success': False, 'code_app': 'C2200', 'text': None}
        args = location_parser.parse_args()

        if args['id'] is None or 'id' not in args:
            return_message['code_app'] = 'C2201'
            return_message['text'] = 'You need to specify an id'
            return return_message, 401

        location = LocationDB().query.filter_by(id=args['id']).first()

        if 'code' in args and args['code'] is not None:
            location.set_code(args['code'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Code change.'
            else:
                return_message['text'] = return_message['text'] + 'Le code est changé.'

        if 'nom' in args and args['nom'] is not None:
            location.set_name(args['nom'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Name change.'
            else:
                return_message['text'] = return_message['text'] + 'Le nom est changé.'

        if 'gare' in args and args['gare'] is not None:
            if args['gare'] is True:
                location.set_gare(args['gare'], get_id_from_token()[0])
                if return_message['text'] is None:
                    return_message['text'] = 'Gare status change.'
                else:
                    return_message['text'] = return_message['text'] + 'Le statut de Gare est changé.'

        if return_message['text'] is None:
            return_message['text'] = 'Nothing to change'

        return_message['success'] = True
        return_message['code_app'] = 'C2201'
        return return_message, 200


@api.route('/<int:location_id>/update/')
@api.route('/<int:location_id>/update')
class LocationUpdateAPI(Resource):
    @api.response(201, 'Modifié')
    @api.response(404, 'Non trouvé')
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Mets à jour un emplacement")
    @roles_required('modo')
    def put(self, location_id):
        return_message = {'success': False, 'code_app': 'C2300', 'text': None}
        location = LocationDB().query.filter_by(id=location_id).first()
        if location is None:
            return_message['text'] = 'L\'emplacement n\'existe pas.'
            return_message['code_app'] = 'C2301'
            return return_message, 404
        result = location.set_postcode(get_id_from_token()[0])
        if result is False:
            return_message['text'] = 'Impossible de trouver le code postal de l\'emplacement.'
            return_message['code_app'] = 'C2302'
            return return_message, 401
        return_message['success'] = True
        return_message['text'] = 'Mise à jour effectuée.'
        return_message['code_app'] = 'C2303'
        return return_message, 200


@api.route('/update/all/')
@api.route('/update/all')
class LocationUpdateAllAPI(Resource):
    @api.response(201, 'Modifié')
    @api.response(404, 'Non trouvé')
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a location")
    @roles_required('modo')
    def put(self):
        return_message = {'success': False, 'code_app': 'C2400', 'text': None}
        if current_app.config['CELERY_ENABLE'] is True:
            update_all_locations.delay(get_id_from_token()[0])
            return_message['code_app'] = 'C2401'
            return_message['text'] = 'Mise à jour effectuée (Asynchrone).'
        else:
            update_all_locations(get_id_from_token()[0])
            return_message['code_app'] = 'C2402'
            return_message['text'] = 'Mise à jour effectuée.'
        return_message['success'] = True
        return return_message, 200
