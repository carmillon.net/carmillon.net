#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import current_app
from flask_restplus import Resource, Namespace
from carmillon.api.common import brands_list_message, auth_token_needed, brand_parser, roles_required, \
    get_id_from_token, message
from carmillon.models.brands import BrandDB
from carmillon.extensions import db, celery

api = Namespace('Enseignes', description='Operations on items')


@api.route('/')
@api.route('')
class BrandsAPI(Resource):
    @api.response(200, 'OK')
    @api.response(404, 'Not found')
    @api.doc("Get the brands")
    @api.doc(security='apikey')
    @api.marshal_with(brands_list_message, envelope='response')
    @auth_token_needed
    @api.expect(brand_parser, validate=True)
    def get(self):
        return_message = {'success': False, 'code_app': 'C2000', 'text': None, 'data': []}
        args = brand_parser.parse_args()
        if args['id'] is not None:
            result = BrandDB().query.filter_by(id=args['id']).all()
        elif args['code'] is not None:
            result = BrandDB().query.filter_by(code_text=args['code']).all()
        elif args['nom'] is not None:
            result = BrandDB().query.filter_by(name_text=args['nom']).all()
        else:
            result = BrandDB().query.order_by("name_text").all()
        for i in result:
            return_message['data'].append(i)
        return_message['success'] = True
        return_message['code_app'] = 'C2001'
        return_message['text'] = 'Search OK'
        return return_message, 200

    @api.response(201, 'Create')
    @api.response(404, 'Not found')
    @api.marshal_with(brands_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a brand")
#    @roles_required('modo')
    @api.expect(brand_parser, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C2100', 'text': None}
        args = brand_parser.parse_args()
        query = db.session.query(BrandDB).filter(BrandDB.search_text.like('%' + args['nom'].lower() + '%')).all()
        current_app.logger.info('post query %s', query)
        if len(query) > 0 or query is None:
            current_app.logger.info('post query 2 %s', query)
            return_message['code_app'] = 'C2105'
            return_message['text'] = 'Erreur, le nom de l\'enseigne existe déjà.'
            return return_message, 401
        query = db.session.query(BrandDB).filter(BrandDB.search_text.like('%' + args['code'].lower() + '%')).all()
        if len(query) > 0 or query is None:
            return_message['code_app'] = 'C2106'
            return_message['text'] = 'Erreur, le code de l\'enseigne existe déjà.'
            return return_message, 401
        if args['code'] is None or 'code' not in args:
            if args['nom'] is None or 'nom' not in args:
                return_message['code_app'] = 'C2101'
                return_message['text'] = 'Erreur, vous devez specifier un code ou un nom'
                return return_message, 401
            BrandDB(name_text=args['nom'], code_text=args['nom']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2102'
            return_message['text'] = 'Ajout OK (avec code manquant)'
            return return_message, 201
        if args['nom'] is None or 'nom' not in args:
            BrandDB(name_text=args['code'], code_text=args['code']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2103'
            return_message['text'] = 'Ajout OK (avec nom manquant)'
            return return_message, 201
        BrandDB(name_text=args['nom'], code_text=args['code']).add()
        return_message['success'] = True
        return_message['code_app'] = 'C2104'
        return_message['text'] = 'Ajout OK'
        return return_message, 201

    @api.response(201, 'Modified')
    @api.response(404, 'Not found')
    @api.marshal_with(brands_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a brand")
    @roles_required('modo')
    @api.expect(brand_parser, validate=True)
    def put(self):
        return_message = {'success': False, 'code_app': 'C2200', 'text': None}
        args = brand_parser.parse_args()

        if args['id'] is None or 'id' not in args:
            return_message['code_app'] = 'C2201'
            return_message['text'] = 'Vous devez spécifier un ID'
            return return_message, 401

        brand = BrandDB().query.filter_by(id=args['id']).first()

        if 'code' in args and args['code'] is not None:
            brand.set_code(args['code'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Code changé.'
            else:
                return_message['text'] = return_message['text'] + 'Code change.'

        if 'nom' in args and args['nom'] is not None:
            brand.set_name(args['nom'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Nom changé.'
            else:
                return_message['text'] = return_message['text'] + ' Nom changé.'

        if return_message['text'] is None:
            return_message['text'] = 'Rien à changer'

        return_message['success'] = True
        return_message['code_app'] = 'C2201'
        return return_message, 200


@api.route('/update/all/')
@api.route('/update/all')
class BrandUpdateAllAPI(Resource):
    @api.response(201, 'Modifié')
    @api.response(404, 'Non trouvé')
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Mets à jour toutes les enseignes")
    @roles_required('modo')
    def put(self):
        return_message = {'success': False, 'code_app': 'C2200', 'text': None}
        if current_app.config['CELERY_ENABLE'] is True:
            update_all_brands.delay(get_id_from_token()[0])
            return_message['code_app'] = 'C2251'
            return_message['text'] = 'Mise à jour effectuée (Asynchrone).'
        else:
            update_all_brands(get_id_from_token()[0])
            return_message['code_app'] = 'C2252'
            return_message['text'] = 'Mise à jour effectuée.'
        return_message['success'] = True
        return return_message, 200


@celery.task
def update_all_brands(user_id):
    brands = BrandDB().query.all()
    for i in brands:
        i.update(user_id)