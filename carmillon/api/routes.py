#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Api
from flask import make_response

from carmillon.api.common import api as ns_common
from carmillon.api.ping import api as ns_ping
from carmillon.api.auth import api as ns_auth
from carmillon.api.db import api as ns_db
from carmillon.api.items import api as ns_items
from carmillon.api.brands import api as ns_brands
from carmillon.api.dept import api as ns_dept
from carmillon.api.locations import api as ns_locations
from carmillon.api.categories import api as ns_categories
from carmillon.api.status import api as ns_status

import io
import csv

version_major = 0
version_minor = 0
version = str(version_major) + '.' + str(version_minor)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authentication-Token',
    }
}
api = Api(
    title='Carmillon DB API',
    version=str(version),
    description='API to access information from the Carmillon DB Database',
    authorizations=authorizations,
)

# API
version_api = 'v' + str(version_major)
suffix_api = '/api/'
suffix_url = suffix_api + version_api


def output_csv(data, code, headers=None):
    """Makes a Flask response with a CSV encoded body"""
    return_list = []
    response = data['response']['data']

    if isinstance(response, list):
        for i in response:
            return_item = []
            for key in i.keys():
                return_item.append(i[key])
            return_list.append(return_item)
    else:
        return_item = []
        for key in response.keys():
            return_item.append(response[key])
        return_list.append(return_item)

    dest = io.StringIO()
    writer = csv.writer(dest, delimiter='|')

    for row in return_list:
        writer.writerow(row)

    resp = make_response(dest.getvalue(), code)
    resp.headers.extend(headers or {})
    return resp


api.representations['text/csv'] = output_csv

api.add_namespace(ns_common, path=suffix_url + '/models')
api.add_namespace(ns_ping, path=suffix_url + '/ping')
api.add_namespace(ns_auth, path=suffix_url + '/auth')
api.add_namespace(ns_db, path=suffix_url + '/db')
api.add_namespace(ns_items, path=suffix_url + '/items')
api.add_namespace(ns_dept, path=suffix_url + '/departements')
api.add_namespace(ns_brands, path=suffix_url + '/enseignes')
api.add_namespace(ns_locations, path=suffix_url + '/emplacements')
api.add_namespace(ns_categories, path=suffix_url + '/categories')
api.add_namespace(ns_status, path=suffix_url + '/statuts')
