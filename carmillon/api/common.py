#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Namespace, fields, reqparse, inputs
from flask_mail import Message
from functools import wraps
from flask import request, current_app
from datetime import datetime, timedelta
import jwt
from werkzeug.datastructures import FileStorage
from itsdangerous import URLSafeTimedSerializer
from sqlalchemy import exc

from carmillon.extensions import db
from carmillon.extensions import mail
from carmillon.models.users_by_roles import UserByRoleDB
from carmillon.models.roles import RoleDB

from carmillon.models.dept import DeptDB
from carmillon.models.locations import LocationDB
from carmillon.models.brands import BrandDB
from carmillon.models.status import StatusDB
from carmillon.models.categories import CategoryDB

api = Namespace('models', description='Models')

empty_list = [None, '', ' ']

user = api.model('User', {
    'id': fields.Integer,
    'email': fields.String,
})


message = api.model('Message', {
    'success': fields.Boolean,
    'code_app': fields.String,
    'text': fields.String,
})

item_data = {
    'id': fields.String(attribute="id"),
    'departement_code': fields.String(attribute="dept_code_text"),
    'departement_name': fields.String(attribute="dept_name_text"),
    'location_city': fields.String(attribute="locations_name_text"),
    'status_name': fields.String(attribute="status_name_text"),
    'status_code': fields.String(attribute="status_code_text"),
    'status_date': fields.String(attribute="status_date"),
    'category_name': fields.String(attribute="categories_name_text"),
    'brand_name': fields.String(attribute="brands_name_text"),
    'location_address': fields.String(attribute="address_text"),
    'avantage_text': fields.String(attribute="description_text"),
    'lat_float': fields.Float(attribute="lat_float"),
    'lon_float': fields.Float(attribute="lon_float"),
    'category_id': fields.String(attribute="category_id"),
    'brand_id': fields.String(attribute="brand_id"),
    'departement_id': fields.String(attribute="dept_id"),
    'location_id': fields.String(attribute="locations_id"),
    'status_id': fields.String(attribute="status_id"),
    'creer_par_id': fields.String(attribute="created_by_id"),
}

list_message = api.clone('Items List', message, {
    'data': fields.List(fields.Nested(item_data))
})

login_message = api.clone('Login Message', message, {
    'data': fields.Nested(api.model('Login', {
        'auth_token': fields.String,
        'user_id': fields.String,
        'user_username': fields.String
    }))
})

brands_list_message = api.clone('Brands List', message, {
    'data': fields.List(fields.Nested(api.model('Brand', {
        'id': fields.String(attribute="id"),
        'code': fields.String(attribute="code_text"),
        'nom': fields.String(attribute="name_text"),
        'type': fields.String(default='enseignes'),
    })))
})

dept_list_message = api.clone('Départements List', message, {
    'data': fields.List(fields.Nested(api.model('Département', {
        'id': fields.String(attribute="id"),
        'code': fields.String(attribute="code_text"),
        'nom': fields.String(attribute="name_text"),
        'type': fields.String(default='departements'),
    })))
})

location_list_message = api.clone('Emplacements List', message, {
    'data': fields.List(fields.Nested(api.model('Emplacement', {
        'id': fields.String(attribute="id"),
        'code': fields.String(attribute="code_text"),
        'nom': fields.String(attribute="name_text"),
        'code_postal': fields.String(attribute="postcode_text"),
        'gare': fields.Boolean(attribute="gare_bool"),
        'type': fields.String(default='emplacements'),
    })))
})

category_list_message = api.clone('Categories List', message, {
    'data': fields.List(fields.Nested(api.model('Categorie', {
        'id': fields.String(attribute="id"),
        'code': fields.String(attribute="code_text"),
        'nom': fields.String(attribute="name_text"),
        'type': fields.String(default='categories'),
    })))
})

status_list_message = api.clone('Status List', message, {
    'data': fields.List(fields.Nested(api.model('Status', {
        'id': fields.String(attribute="id"),
        'code': fields.String(attribute="code_text"),
        'nom': fields.String(attribute="name_text"),
        'type': fields.String(default='statuts'),
    })))
})

comments_list_message = api.clone('Comments List', message, {
    'data': fields.Nested(api.model('List Comments', {
        'commentaires': fields.List(fields.Nested(api.model('Comment', {
            'id': fields.String(attribute="id"),
            'message': fields.String(attribute="message_text"),
            'validation': fields.Boolean(attribute="validation_bool"),
            'active': fields.Boolean(attribute='active_bool'),
            'cree_par_id': fields.String(attribute="created_by_id"),
            'cree_le': fields.DateTime(attribute='created_time'),
            'cree_par': fields.String(attribute='created_by_username'),
            'modifie_par': fields.String(attribute='modified_by_username'),
            'modifie_le': fields.DateTime(attribute='modified_time'),
            'alerte': fields.Boolean(attribute='report_bool'),
            'alerte_le': fields.DateTime(attribute='report_time'),
            'alerte_par': fields.String(attribute='report_by_username'),
        }))),
        'nombre': fields.Integer,
        'validations': fields.Integer,
        'non_validations': fields.Integer,
        'derniere_validation': fields.DateTime
    }))
})

item_export = api.model('Items export', item_data)

upload_parser = reqparse.RequestParser()
upload_parser.add_argument('files',
                           type=FileStorage,
                           location='files',
                           required=True)

list_parser = reqparse.RequestParser()

list_parser.add_argument('dept',
                         type=str,
                         help='Département pour filtrer')

list_parser.add_argument('ville',
                         type=str,
                         help='Ville / Gare pour filter')

list_parser.add_argument('enseigne',
                         type=str,
                         help='Enseigne pour filter')

list_parser.add_argument('statut',
                         type=str,
                         help='Statut')

list_parser.add_argument('description',
                         type=str,
                         help='Description')

list_parser.add_argument('adresse',
                         type=str,
                         help='Adresse')

list_parser.add_argument('categorie',
                         type=str,
                         help='Category')

list_parser.add_argument('recherche',
                         type=str,
                         help='Filtrer par recherche')

list_parser.add_argument('id',
                         type=int,
                         help='Recherche par id')

list_parser.add_argument('par_statuts',
                         type=str,
                         help='Recherche par statuts: '
                              'tous = Tous les items, '
                              'confirmes = tous les items confirmés et ses items autres')

brand_parser = reqparse.RequestParser()
brand_parser.add_argument('id',
                          type=str,
                          help='ID de l\'enseigne')
brand_parser.add_argument('code',
                          type=str,
                          help='Code de l\'enseigne')
brand_parser.add_argument('nom',
                          type=str,
                          help='Nom de l\'enseigne')

dept_parser = reqparse.RequestParser()
dept_parser.add_argument('id',
                         type=str,
                         help='ID du département')
dept_parser.add_argument('code',
                         type=str,
                         help='Code du département')
dept_parser.add_argument('nom',
                         type=str,
                         help='Nom du département')

category_parser = reqparse.RequestParser()
category_parser.add_argument('id',
                             type=str,
                             help='ID de la catégorie')
category_parser.add_argument('code',
                             type=str,
                             help='Code de la catégorie')
category_parser.add_argument('nom',
                             type=str,
                             help='Nom de la catégorie')

location_parser = reqparse.RequestParser()
location_parser.add_argument('id',
                             type=str,
                             help='ID de la localisation')
location_parser.add_argument('code',
                             type=str,
                             help='Code de la localisation')
location_parser.add_argument('nom',
                             type=str,
                             help='Nom de la localisation')
location_parser.add_argument('gare',
                             type=bool,
                             help='Est-ce une gare ?')

status_parser = reqparse.RequestParser()
status_parser.add_argument('id',
                           type=str,
                           help='ID du status')
status_parser.add_argument('code',
                           type=str,
                           help='Code du status')
status_parser.add_argument('nom',
                           type=str,
                           help='Nom du status')

comments_parser = reqparse.RequestParser()
comments_parser.add_argument('id',
                             type=str,
                             help='ID du commentaire')
comments_parser.add_argument('message',
                             type=str,
                             help='Message du commentaire')
comments_parser.add_argument('validation',
                             type=bool,
                             help='Validation ou non de l item')
comments_parser.add_argument('date',
                             type=inputs.date_from_iso8601,
                             help='Date format YYYY-mm-DD')
comments_parser.add_argument('activer',
                             type=bool,
                             help='Validation ou non de l item')
comments_parser.add_argument('alerte',
                             type=bool,
                             help='Signaler le commentaire')


def parse_arg(description=None, statut=None, dept=None, dept_code=None, enseigne=None, ville=None, adresse=None,
              categorie=None, statut_date=None, source=None):
    return_message = {'success': False, 'code_app': 'C0400', 'text': '', 'data': []}
    if description is None:
        args = list_parser.parse_args()
    else:
        args = {'description': description, 'statut': statut, 'dept': dept, 'ville': ville, 'enseigne': enseigne,
                'adresse': adresse, 'categorie': categorie}
    dept_id = None
    dept = args['dept']
    if dept not in empty_list:
        if isinstance(dept, int):
            test = DeptDB.query.filter_by(id=dept).first()
            if test is not None:
                dept_id = test.id
        else:
            test = DeptDB.query.filter_by(name_text=dept).first()
            if test is not None:
                dept_id = test.id
            else:
                test = DeptDB.query.filter_by(code_text=dept).first()
                if test is not None:
                    dept_id = test.id
                else:
                    if dept_code is None:
                        DeptDB(name_text=dept, code_text=dept).add()
                    else:
                        DeptDB(name_text=dept, code_text=dept_code).add()
                    test = DeptDB.query.filter_by(name_text=dept).first()
                    dept_id = test.id
    location_id = None
    location = args['ville']
    if location not in empty_list:
        if isinstance(location, int):
            test = LocationDB.query.filter_by(id=location).first()
            if test is not None:
                location_id = test.id
        else:
            test = LocationDB.query.filter_by(name_text=location).first()
            if test is not None:
                location_id = test.id
            else:
                test = LocationDB.query.filter_by(code_text=location).first()
                if test is not None:
                    location_id = test.id
                else:
                    LocationDB(name_text=location, code_text=location).add()
                    test = LocationDB.query.filter_by(name_text=location).first()
                    location_id = test.id
    brand_id = None
    brand = args['enseigne']
    if brand not in empty_list:
        if isinstance(brand, int):
            test = BrandDB.query.filter_by(id=brand).first()
            if test is not None:
                brand_id = test.id
        else:
            test = BrandDB.query.filter_by(name_text=brand).first()
            if test is not None:
                brand_id = test.id
            else:
                test = BrandDB.query.filter_by(code_text=brand).first()
                if test is not None:
                    brand_id = test.id
                else:
                    BrandDB(name_text=brand, code_text=brand).add()
                    test = BrandDB.query.filter_by(name_text=brand).first()
                    brand_id = test.id
    status_id = None
    status = args['statut']
    if status not in empty_list:
        if isinstance(status, int):
            test = StatusDB.query.filter_by(id=status).first()
            if test is not None:
                status_id = test.id
        else:
            test = StatusDB.query.filter_by(name_text=status).first()
            if test is not None:
                status_id = test.id
            else:
                test = StatusDB.query.filter_by(code_text=status).first()
                if test is not None:
                    status_id = test.id
                else:
                    StatusDB(name_text=status, code_text=status).add()
                    test = StatusDB.query.filter_by(name_text=status).first()
                    status_id = test.id
    category_id = None
    category = args['categorie']
    if category not in empty_list:
        if isinstance(category, int):
            test = CategoryDB.query.filter_by(id=category).first()
            if test is not None:
                category_id = test.id
        else:
            test = CategoryDB.query.filter_by(name_text=category).first()
            if test is not None:
                category_id = test.id
            else:
                test = CategoryDB.query.filter_by(code_text=category).first()
                if test is not None:
                    category_id = test.id
                else:
                    CategoryDB(name_text=category, code_text=category).add()
                    test = CategoryDB.query.filter_by(name_text=category).first()
                    category_id = test.id

    date_time = None
    if statut_date not in (None, "", " "):
        split_status_date = statut_date.split("/")
        if len(split_status_date[2]) == 4:
            date_time = datetime.strptime(statut_date, '%d/%m/%Y')
        else:
            new_date = split_status_date[0] + "/" + split_status_date[1] + "/" + "20" + split_status_date[2]
            date_time = datetime.strptime(new_date, '%d/%m/%Y')

    if 'recherche' in args:
        recherche = args['recherche']
    else:
        recherche = None

    if 'id' in args:
        id_item = args['id']
    else:
        id_item = None

    if 'par_statuts' in args:
        par_statuts = args['par_statuts']
    else:
        par_statuts = None

    return return_message, {'dept': dept_id, 'ville': location_id, 'enseigne': brand_id, 'statut': status_id,
                            'statut_date': date_time, 'categorie': category_id, 'adresse': args['adresse'],
                            'description': args['description'], 'recherche': recherche, 'id': id_item,
                            'source': source, 'par_statuts': par_statuts}


def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    payload = {
        'exp': datetime.utcnow() + timedelta(days=1, seconds=0),
        'iat': datetime.utcnow(),
        'sub': user_id
    }
    return jwt.encode(
        payload,
        current_app.config.get('SECRET_KEY'),
        algorithm='HS256'
    ).decode('utf-8')


def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token,
                             current_app.config.get('SECRET_KEY'),
                             algorithms=['HS256'])
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email


def send_email(to, subject, template):
    if current_app.config['CONFIG_TYPE'] == 'TEST':
        current_app.logger.info('Mail envoyé')
        return True
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=current_app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)


def get_id_from_token():
    auth_header = request.headers.get('Authentication-Token')
    if auth_header:
        auth_token = auth_header
    else:
        auth_token = ''
    if auth_token:
        resp = decode_auth_token(auth_token)
        if not isinstance(resp, str):
            return resp, "OK"
        else:
            return None, 'Authentication-Token header is incorrect'
    return None, 'Authentication-Token header is missing'


def auth_token_needed(func):
    """Checks whether the Authorization-Token is valid or raises error 401."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        resp, return_text = get_id_from_token()
        if resp is not None:
            return func(*args, **kwargs)
        else:
            api.abort(401, return_text)
    return wrapper


def roles_required(role):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            resp, return_text = get_id_from_token()
            if resp is not None:
                try:
                    query = db.session.query(UserByRoleDB) \
                        .join(RoleDB) \
                        .filter(UserByRoleDB.user_id == resp) \
                        .filter(RoleDB.code_text == role) \
                        .first()
                except exc.OperationalError:
                    api.abort(500, 'Erreur de connexion à la base de données')
                if query is not None:
                    return func(*args, **kwargs)
                else:
                    api.abort(401, 'L\'utilisateur n\'a pas le rôle nécessaire à cette action.')
            else:
                api.abort(401, return_text)

        return wrapper
    return decorator


def get_user_id_from_token():
    auth_token = request.headers.get('Authentication-Token')
    if auth_token:
        return decode_auth_token(auth_token)
    return None


def have_role(role):
    test = db.session.query(UserByRoleDB) \
            .join(RoleDB) \
            .filter(UserByRoleDB.user_id == get_id_from_token()[0]) \
            .filter(RoleDB.code_text == role) \
            .first()
    if test is not None:
        return True
    return False
