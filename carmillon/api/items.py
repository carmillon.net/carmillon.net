#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import current_app, request
from flask_restplus import Resource, Namespace
from sqlalchemy import or_
from carmillon.api.common import message, auth_token_needed, roles_required, list_message, list_parser, parse_arg, \
    upload_parser, item_export, get_id_from_token, comments_parser, comments_list_message, have_role, empty_list
from carmillon.models.items import ItemDB
from carmillon.models.dept import DeptDB
from carmillon.models.locations import LocationDB
from carmillon.models.status import StatusDB
from carmillon.models.brands import BrandDB
from carmillon.models.categories import CategoryDB
from carmillon.models.comments import CommentsDB
from carmillon.models.comments_by_items import CommentsByItemsDB
from carmillon.extensions import db, celery

import csv
import io

from datetime import datetime

api = Namespace('Items', description='Operations sur les items')


@celery.task
def update_items(user_id):
    current_app.logger.warning('Enter update_items')
    items = ItemDB.query.all()
    for i in items:
        i.update(user_id)
    return True


class GenericItemAPI(Resource):
    def _post(self, dept_code=None, dept=None, ville=None, statut=None, statut_date=None, enseigne=None,
              description=None, adresse=None, categorie=None, source=None, update=True):
        return_message, args = parse_arg(description=description, statut=statut, statut_date=statut_date, dept=dept,
                                         dept_code=dept_code, enseigne=enseigne,
                                         ville=ville, adresse=adresse, categorie=categorie,
                                         source=source)

        item = None
        if args['dept'] is not None and args['ville'] is not None:
            item = ItemDB.query.filter_by(dept_id=args['dept'],
                                          brand_id=args['enseigne'],
                                          location_id=args['ville']).first()

        if item is None:
            if args['description'] in empty_list:
                return_message['text'] = 'Le champ description doit être renseigné.'
                return_message['code_app'] = 'C6001'
                return return_message, 401
            if args['adresse'] in empty_list:
                return_message['text'] = 'Le champ adresse doit être renseigné.'
                return_message['code_app'] = 'C6002'
                return return_message, 401
            if args['categorie'] in empty_list:
                return_message['text'] = 'Le champ categorie doit être renseigné.'
                return_message['code_app'] = 'C6003'
                return return_message, 401
            if args['enseigne'] in empty_list:
                return_message['text'] = 'Le champ enseigne doit être renseigné.'
                return_message['code_app'] = 'C6004'
                return return_message, 401
            if have_role('modo'):
                item = ItemDB(description_text=args['description'],
                              dept_id=args['dept'],
                              brand_id=args['enseigne'],
                              location_id=args['ville'],
                              address_text=args['adresse'],
                              category_id=args['categorie'],
                              source_text=args['source'])
                item.add(status_code='CONFIRME-01', update=update, user_id=get_id_from_token()[0])
                return_message['code_app'] = 'C6006'
                return_message['text'] = 'L\'avantage a été créé.'
                return return_message, 201
            item = ItemDB(description_text=args['description'],
                          dept_id=args['dept'],
                          brand_id=args['enseigne'],
                          location_id=args['ville'],
                          address_text=args['adresse'],
                          category_id=args['categorie'],
                          source_text=args['source'])
            item.add(status_code='PROPOSE-01', update=update, user_id=get_id_from_token()[0])
            return_message['code_app'] = 'C6005'
            return_message['text'] = 'L\'avantage a été créé, en attente de validation par un modérateur.'
            return return_message, 201

        return return_message, 200

    def _format_result(self, result, return_message):
        for i in result:
            to_add = i[0].format()
            if to_add["dept_code_text"] is None:
                to_add["dept_code_text"] = i[1]
            if to_add["dept_name_text"] is None:
                to_add["dept_name_text"] = i[2]
            if to_add["locations_name_text"] is None:
                to_add["locations_name_text"] = i[3]
            to_add["status_name_text"] = i[4]
            to_add["categories_name_text"] = i[5]
            to_add["brands_name_text"] = i[6]
            to_add["status_code_text"] = i[7]
            return_message['data'].append(to_add)
        return return_message

    def _gen_query(self):
        query = db.session.query(ItemDB,
                                 DeptDB.code_text,
                                 DeptDB.name_text,
                                 LocationDB.name_text,
                                 StatusDB.name_text,
                                 CategoryDB.name_text,
                                 BrandDB.name_text,
                                 StatusDB.code_text,
                                 ) \
            .outerjoin(ItemDB.status) \
            .outerjoin(ItemDB.dept) \
            .outerjoin(ItemDB.brand) \
            .outerjoin(ItemDB.location) \
            .outerjoin(ItemDB.category)
        return query

    def _query_recherche(self, query, recherche):
        if recherche is not None:
            list_recherche = recherche.split(" ")
            for i in list_recherche:
                query = query.filter(ItemDB.search_text.like('%' + i.lower() + '%'))
        return query


@api.route('/')
@api.route('')
class ItemsAPI(GenericItemAPI):
    @api.response(200, 'OK')
    @api.response(201, 'Created OK')
    @auth_token_needed
#    @roles_required('modo')
    @api.doc(security='apikey')
    @api.doc("Créer un avantage")
    @api.expect(list_parser, validate=True)
    @api.marshal_with(message, envelope='response')
    def post(self):
        return_message, code = self._post()
        if code == 201:
            return_message['success'] = True
            return return_message, code
        if code == 200:
            return_message['success'] = True
            return return_message, code
        else:
            return return_message, code

    @api.response(200, 'OK')
    @api.response(201, 'Updated OK')
    @auth_token_needed
    @roles_required('modo')
    @api.doc(security='apikey')
    @api.doc("Update an element")
    @api.expect(list_parser, validate=True)
    @api.marshal_with(message, envelope='response')
    def put(self):
        return_message, args = parse_arg()
        if args['id'] is None:
            return_message['code_app'] = 'C1101'
            return_message['text'] = 'ID donné incorrect'
            return return_message, 404
        item = ItemDB.query.filter_by(id=args['id']).first()
        if item is None:
            return_message['code_app'] = 'C1102'
            return_message['text'] = 'ID n\'existe pas'
            return return_message, 404

        item.set_description(args['description'], get_id_from_token()[0])
        item.set_address(args['adresse'], get_id_from_token()[0])
        item.set_category(get_id_from_token()[0], category_id=args['categorie'])

        item.set_status(get_id_from_token()[0], status_id=args['statut'])
        item.set_dept(get_id_from_token()[0], dept_id=args['dept'])

        item.set_brand(get_id_from_token()[0], brand_id=args['enseigne'])
        item.set_location(get_id_from_token()[0], location_id=args['ville'],)
        item.update(user_id=get_id_from_token()[0])
        return_message['success'] = True
        return_message['code_app'] = 'C1102'
        return_message['text'] = 'Enregistrement effectué'
        return return_message, 200

    @api.response(200, 'OK')
    @api.response(404, 'Not found')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.expect(list_parser, validate=True)
    @api.doc("Get a list")
    @api.marshal_with(list_message, envelope='response')
    def get(self):
        return_message, args = parse_arg()
        query = self._gen_query()
        if args['id'] is not None:
            query = query.filter(ItemDB.id == args['id'])
            result = query.all()
            result_filter = []
            for i in result:
                if have_role('modo'):
                    result_filter.append(i)
                elif i[0].get_status_code() != "PROPOSE-01":
                    result_filter.append(i)
                elif get_id_from_token()[0] == i[0].created_by_id:
                    result_filter.append(i)
            return_message = self._format_result(result_filter, return_message)
            return_message['success'] = True
            return_message['code_app'] = 'C1002'
            return_message['text'] = 'Select id OK'
            return return_message, 200
        query = self._query_recherche(query, args['recherche'])
        if args["par_statuts"] is not None:
            if args["par_statuts"] == "confirmes":
                query = query.filter(or_(StatusDB.code_text == "CONFIRME-01",
                                         ItemDB.created_by_id == get_id_from_token()[0]))
        else:
            if have_role('modo') is False:
                query = query.filter(or_(StatusDB.code_text != "PROPOSE-01",
                                         ItemDB.created_by_id == get_id_from_token()[0]))
        result = query.all()
        return_message = self._format_result(result, return_message)
        return_message['success'] = True
        return_message['code_app'] = 'C1001'
        return_message['text'] = 'Recherche OK'
        return return_message, 200

    @api.response(200, 'OK')
    @api.response(204, 'OK')
    @auth_token_needed
    @roles_required('admin')
    @api.doc(security='apikey')
    @api.doc("Delete an element")
    @api.expect(list_parser, validate=True)
    @api.marshal_with(message, envelope='response')
    def delete(self):
        return_message, args = parse_arg()
        if args['id'] is None:
            return_message['code_app'] = 'C1101'
            return_message['text'] = 'ID donné incorrect'
            return return_message, 404
        item = ItemDB.query.filter_by(id=args['id']).first()
        if item is None:
            return_message['code_app'] = 'C1102'
            return_message['text'] = 'ID n\'existe pas'
            return return_message, 404
        item.delete()
        return_message['success'] = True
        return_message['code_app'] = 'C1102'
        return_message['text'] = 'Suppression effectuée'
        return return_message, 200


@api.route('/import/')
@api.route('/import')
class ItemImportAPI(GenericItemAPI):
    @api.response(200, 'OK')
    @auth_token_needed
    @roles_required('admin')
    @api.doc(security='apikey')
    @api.doc("Import items")
    @api.marshal_list_with(list_message, envelope='response')
    @api.expect(upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        uploaded_file = None
        if 'files' in args.keys():
            uploaded_file = args['files']
        else:
            api.abort(401)
        return_list = []
        csvfile = io.StringIO(uploaded_file.stream.read().decode("UTF8"), newline='')
        spamreader = csv.reader(csvfile, delimiter='|')

        all_delete = ItemDB.query.filter_by(source_text='import_histo').all()
        for i in all_delete:
            i.delete()

        for row in spamreader:
            price_db = self._post(dept_code=row[0], dept=row[1], ville=row[2], statut=row[3], statut_date=row[4],
                                  categorie=row[5], enseigne=row[6],
                                  adresse=row[7], description=row[8],
                                  source='import_histo',
                                  update=False)
            return_list.append(price_db)
        return return_list, 200


@api.route('/export/')
@api.route('/export')
class ItemExportAPI(GenericItemAPI):
    @api.response(200, 'OK')
    @auth_token_needed
    @roles_required('modo')
    @api.doc(security='apikey')
    @api.doc("Export items")
    @api.expect(list_parser, validate=True)
    @api.marshal_list_with(item_export, envelope='response')
    def get(self):
        return_message, args = parse_arg()
        query = self._gen_query()
        query = self._query_recherche(query, args['recherche'])
        if args['recherche'] is not None:
            list_recherche = args['recherche'].split(" ")
            for i in list_recherche:
                query = query.filter(ItemDB.search_text.like('%' + i.lower() + '%'))
        result = query.all()
        return_message = self._format_result(result, return_message)
        return_message['success'] = True
        return_message['code_app'] = 'C2001'
        return_message['text'] = 'Export OK'
        return return_message['data'], 200


@api.route('/update/all/')
@api.route('/update/all')
class UpdateAllAPI(GenericItemAPI):
    @api.response(200, 'OK')
    @auth_token_needed
    @roles_required('admin')
    @api.doc(security='apikey')
    @api.doc("Update all items")
    @api.marshal_with(message, envelope='response')
    def put(self):
        return_message = {'success': False, 'code_app': 'C0100', 'text': None}
        user_id = get_id_from_token()[0]
        status = False
        if current_app.config['CELERY_ENABLE'] is True:
            status = update_items.delay(user_id)
            return_message['text'] = 'Mise à jour lancée.'
            return return_message, 200
        else:
            status = update_items(user_id)
            return_message['text'] = 'Mise à jour effectuée.'
        if status is True:
            return_message['success'] = True
            return_message['code_app'] = 'C2101'
            return return_message, 200
        return_message['code_app'] = 'C2102'
        return_message['text'] = 'Problème lors de la mise à jour.'
        return return_message, 500


@api.route('/<int:item_id>/update/')
@api.route('/<int:item_id>/update')
class UpdateAPI(GenericItemAPI):
    @api.response(200, 'OK')
    @auth_token_needed
    @roles_required('modo')
    @api.doc(security='apikey')
    @api.doc("Update one item")
    @api.marshal_with(message, envelope='response')
    def put(self, item_id):
        return_message = {'success': False, 'code_app': 'C2200', 'text': None}
        user_id = get_id_from_token()[0]
        item = ItemDB.query.filter_by(id=item_id).first()
        if item is None:
            return_message['code_app'] = 'C2201'
            return_message['text'] = 'Avantage non trouvé.'
            return return_message, 404
        item.update(user_id)
        return_message['success'] = True
        return_message['code_app'] = 'C2202'
        return_message['text'] = 'Mise à jour effectuée.'
        return return_message, 200


@api.route('/<int:item_id>/commentaires/')
@api.route('/<int:item_id>/commentaires')
@api.route('/<int:item_id>/commentaires/<int:comment_id>/')
@api.route('/<int:item_id>/commentaires/<int:comment_id>')
class CommentsAPI(Resource):
    def _get_comments(self, item_id):
        query = db.session.query(CommentsDB) \
            .join(CommentsByItemsDB, CommentsByItemsDB.comment_id == CommentsDB.id) \
            .filter(CommentsByItemsDB.item_id == item_id) \
            .filter(CommentsDB.active_bool) \
            .order_by(CommentsDB.created_time.desc())
        return query.all()

    def _get_last_valid(self, result):
        valid = 0
        last_valid = datetime(2000, 1, 1)
        for i in result:
            if i.validation_bool is True:
                valid = valid + 1
                if i.validation_date > last_valid:
                    last_valid = i.validation_date
        if last_valid == datetime(2000, 1, 1):
            last_valid = None
        return valid, last_valid

    @api.response(200, 'OK')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.doc("Creer un commentaire")
    @api.expect(comments_parser, validate=True)
    @api.marshal_with(comments_list_message, envelope='response')
    def post(self, item_id):
        return_message = {'success': False, 'code_app': 'C6000', 'text': '', 'data': []}
        args = comments_parser.parse_args()
        if args['message'] is None and args['validation'] is None:
            return_message['text'] = 'Validation ou message manquant'
            return_message['code_app'] = 'C6003'
            return return_message, 401
        user_token = get_id_from_token()[0]
        comment_by = CommentsByItemsDB.query.filter_by(item_id=item_id, created_by_id=user_token).first()
        if comment_by is not None:
            return_message['text'] = 'Il existe déjà un commentaire pour cet avantage'
            return_message['code_app'] = 'C6004'
            return return_message, 401
        comment = CommentsDB()
        user_id = get_id_from_token()[0]
        result = comment.add(message=args['message'],
                             validation=args['validation'],
                             date=args['date'],
                             user_id=user_id)
        if result is not True:
            return_message['text'] = 'Problème lors de la creation du commentaire.'
            return return_message, 401
        CommentsByItemsDB().add(comment.id, item_id, user_id)
        return_message['success'] = True
        return_message['code_app'] = 'C6001'
        return_message['text'] = 'Commentaire posté avec succes'
        return return_message, 200

    @api.response(200, 'OK')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.doc("Recuperer les commentaires")
    @api.marshal_list_with(comments_list_message, envelope='response')
    def get(self, item_id, comment_id=None):
        return_message = {'success': False, 'code_app': 'C6000', 'text': '', 'data': None}
        if comment_id is not None:
            comment = CommentsDB.query.filter_by(id=comment_id).first()
            comment_by = CommentsByItemsDB.query.filter_by(item_id=item_id, comment_id=comment_id).first()
            if comment is None or comment_by is None:
                return_message['code_app'] = 'C6104'
                return_message['text'] = 'Pas de commentaire'
                return return_message, 404
            return_message['data'] = {'commentaires': comment}
            return_message['success'] = True
            return_message['code_app'] = 'C6102'
            return_message['text'] = 'Commentaire recupere avec succes'
            return return_message, 200

        result = self._get_comments(item_id)
        if result is None:
            return_message['code_app'] = 'C6104'
            return_message['text'] = 'Pas de commentaire'
            return return_message, 404
        comment_list = []
        for i in result:
            comment_list.append(i.format())
        return_message['data'] = {'commentaires': comment_list}
        return_message['data']['nombre'] = len(result)
        valid, last_valid = self._get_last_valid(result)
        return_message['data']['validations'] = valid
        return_message['data']['non_validations'] = len(result) - valid
        return_message['data']['derniere_validation'] = last_valid
        return_message['success'] = True
        return_message['code_app'] = 'C6101'
        return_message['text'] = 'Commentaires recuperes avec succes'
        return return_message, 200

    @api.response(200, 'OK')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.doc("Creer un commentaire")
    @api.expect(comments_parser, validate=True)
    @api.marshal_with(comments_list_message, envelope='response')
    def put(self, item_id, comment_id=None):
        return_message = {'success': False, 'code_app': 'C6000', 'text': '', 'data': []}
        args = comments_parser.parse_args()
        if comment_id is None:
            comment_id = args["id"]
        if comment_id is None:
            return_message['code_app'] = 'C6215'
            return_message['text'] = 'Erreur, l\'id du commentaire doit être spécifié'
            return return_message, 401
        comment = CommentsDB.query.filter_by(id=comment_id).first()
        if comment is None:
            return_message['code_app'] = 'C6216'
            return_message['text'] = 'Erreur, le commentaire n\'existe pas.'
            return return_message, 404
        comment_by = CommentsByItemsDB.query.filter_by(item_id=item_id, comment_id=comment_id).first()
        user_token = get_id_from_token()[0]
        if args['alerte'] is not None:
            report = comment.get_report()
            if report['report'] is False and args['alerte'] is True:
                comment.set_report(report=args['alerte'], report_by=user_token)
                return_message['success'] = True
                return_message['code_app'] = 'C6213'
                return_message['text'] = 'Alerte sur le commentaire faite'
                return_message['data'] = {'commentaires': comment.format()}
                return return_message, 200
            if report['report'] is True and args['alerte'] is False and report['report_by_id'] == user_token:
                comment.set_report(report=args['alerte'], report_by=user_token)
                return_message['success'] = True
                return_message['code_app'] = 'C6214'
                return_message['text'] = 'Alerte annule par l auteur sur le commentaire'
                return_message['data'] = {'commentaires': comment.format()}
                return return_message, 200
            if have_role('modo') is True or have_role('admin') is True:
                comment.set_report(report=args['alerte'], report_by=user_token)
                return_message['success'] = True
                return_message['code_app'] = 'C6215'
                return_message['text'] = 'Alerte modifié par un modérateur ou un admin'
                return_message['data'] = {'commentaires': comment.format()}
        if comment.created_by_id != user_token:
            if have_role('modo') is False and have_role('admin') is False:
                return_message['code_app'] = 'C6212'
                return_message['text'] = 'Erreur, vous n avez pas le droit de modifier ce commentaire'
                return return_message, 403
        if comment is None or comment_by is None:
            return_message['code_app'] = 'C6104'
            return_message['text'] = 'Pas de commentaire'
            return return_message, 404
        if args['message'] is None and args['validation'] is None and args['activer'] is None:
            return_message['code_app'] = 'C6211'
            return_message['text'] = 'Erreur, un message ou une validation doit être spécifié'
            return return_message, 401
        if args['message'] is not None:
            comment.set_message(message=args['message'], user_id=user_token)
        if args['validation'] is not None:
            comment.set_validation(validation=args['validation'], user_id=user_token)
        if args['activer'] is not None:
            if have_role('modo') is False and have_role('admin') is False:
                return_message['code_app'] = 'C6212'
                return_message['text'] = 'Erreur, vous n avez pas le droit de modifier ce commentaire'
                return return_message, 403
            comment.set_active(active=args['activer'], user_id=user_token)
        result = self._get_comments(item_id)
        if result is None:
            return_message['code_app'] = 'C6104'
            return_message['text'] = 'Pas de commentaire'
            return return_message, 404
        return_message['success'] = True
        return_message['code_app'] = 'C6201'
        if args['message'] is not None and args['message'] != '':
            return_message['text'] = 'Commentaire modifié avec succès'
        else:
            return_message['text'] = 'Validation modifiée avec succès'
        return_message['data'] = {'commentaires': comment.format()}
        return_message['data']['nombre'] = len(result)
        valid, last_valid = self._get_last_valid(result)
        return_message['data']['validations'] = valid
        return_message['data']['non_validations'] = len(result) - valid
        return_message['data']['derniere_validation'] = last_valid
        return return_message, 200
