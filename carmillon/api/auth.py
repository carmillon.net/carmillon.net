#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Resource, Namespace, reqparse
from flask import url_for, current_app

from carmillon.extensions import celery, db

from carmillon.api.common import message, login_message, encode_auth_token, auth_token_needed, roles_required, \
    get_id_from_token, generate_confirmation_token, confirm_token, send_email, user

from carmillon.models.users import UserDB
from carmillon.models.roles import RoleDB
from carmillon.models.users_by_roles import UserByRoleDB

import time

api = Namespace('Users', description='Operations on users')

user_parser_email = reqparse.RequestParser()

user_parser_email.add_argument('email',
                               type=str,
                               help='Email of the user',
                               required=True)

user_parser = user_parser_email.copy()
user_parser.add_argument('password',
                         type=str,
                         help='Password to set',
                         required=True)

user_parser_register = user_parser.copy()
user_parser_register.add_argument('confirm_link',
                         type=str,
                         help='Link to send the token')

user_parser_password = reqparse.RequestParser()
user_parser_password.add_argument('password',
                                  type=str,
                                  help='Password to set',
                                  required=True)

user_parser_role = user_parser_email.copy()
user_parser_role.add_argument('role',
                              type=str,
                              help='Role to add',
                              required=True)


@celery.task
def send_email_async(email, subject, html):
    send_email(email, subject, html)


class CommonRegisterAPI(Resource):
    def _send_mail(self, email, confirm_link=None):
        token = generate_confirmation_token(email)
        if confirm_link is None:
            confirm_link = url_for('doc', _external=True) + "api/v0/auth/confirm/"
        confirm_url = confirm_link + token
        html = "<p>Bienvenue !</p>" \
               "<p>Merci de vous être inscrit.</p>" \
               "<p>Merci de confirmer votre compte en naviguant sur le lien ci-dessous :</p>" \
               "<p><a href=" + confirm_url + ">" + confirm_url + "</a></p>" \
                                                                 "<br><p>A bientôt!</p>"
        subject = "Avantages Pass Carmillon - Merci de confirmer votre mail"
        if current_app.config['CELERY_ENABLE'] is True:
            send_email_async(email, subject, html)
        else:
            send_email(email, subject, html)

    def _send_reset_password(self, user_db):
        password = user_db.reset_password()
        html = "<p>Bonjour !</p>" \
               "<p>Votre nouveau mot de passe est : " + password + "</p>" \
               "<p>Pensez à le changer à votre prochaine connexion.</p>"
        subject = "Avantages Pass Carmillon - Nouveau mot de passe"
        if current_app.config['CELERY_ENABLE'] is True:
            send_email_async(user_db.email, subject, html)
        else:
            send_email(user_db.email, subject, html)

    def _post(self, args, restricted=True):
        return_message = {'success': False, 'code_app': 'C0100', 'text': None}
        user_db = UserDB.query.filter_by(email=args['email']).first()
        if user_db is None:
            user_db = UserDB.query.filter_by(email=args['email'].lower()).first()
        if user_db is not None:
            return_message['code_app'] = 'C0102'
            return_message['text'] = 'Le mail est déjà enregistré'
            return return_message, 202
        user = UserDB(email=args['email'].lower())
        status = user.add(password=args['password'], restricted=restricted)

        if status[0] is True:
            if restricted is True:
                self._send_mail(user.email, args['confirm_link'])
            return_message['success'] = True
            return_message['code_app'] = 'C0101'
            return_message['text'] = 'Enregistrement: OK. Un mail a été envoyé pour confirmer le compte.'
            return return_message, 200
        return_message['code_app'] = 'C0103'
        if status[1] is not None:
            return_message['text'] = status[1]
            return return_message, 401
        else:
            return_message['text'] = 'Problème durant la création de l\'utilisateur'
        return return_message, 500


@api.route('/register')
class AuthRegisterAPI(CommonRegisterAPI):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Register a user")
    @api.expect(user_parser_register, validate=True)
    def post(self):
        args = user_parser_register.parse_args()
        return self._post(args=args, restricted=True)


@api.route('/reset_password')
class ResetPasswordAPI(CommonRegisterAPI):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Reinitialiser le mot de passe")
    @api.expect(user_parser_email, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C0120', 'text': None}
        args = user_parser_email.parse_args()
        user_db = UserDB.query.filter_by(email=args['email']).first()
        if user_db is None:
            return_message['code_app'] = 'C0121'
            return_message['text'] = 'Erreur l\'email n\'existe pas.'
            return return_message, 404
        role = db.session.query(UserByRoleDB) \
            .join(RoleDB) \
            .filter(UserByRoleDB.user_id == user_db.id) \
            .filter(RoleDB.code_text == 'admin') \
            .first()
        if role is not None:
            return_message['code_app'] = 'C0122'
            return_message['text'] = 'Erreur, impossible à reinitialiser'
            return return_message, 401
        self._send_reset_password(user_db)
        return_message['success'] = True
        return_message['code_app'] = 'C0122'
        return_message['text'] = 'Un mail a été envoyé.'
        return return_message, 200


@api.route('/conditions')
class ConditionsAPI(Resource):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Get the condition to use the service")
    def get(self):
        text = "<p>Vous êtes responsable des messages que vous publiez sur le site. Tout message contraire à " \
               "l'utilisation normale du site sera supprimé.\n</p>" \
               "<p>Des cookies sont utilisés pour stocker les informations techniques de connexion, et ne sont pas " \
               "utilisés à des fins publicitaires ou de tracking.\n</p>" \
               "<p>Les informations enregistrés sur le site ne sont pas utilisées, ni transmis à des tiers, à des fins " \
               "publicitaires.</p>"
        return {'success': True, 'code_app': 'C0001', 'text': text}, 200


@api.route('/register_restricted')
class AuthRegisterRestrictedAPI(CommonRegisterAPI):
    @api.response(200, 'OK')
    @roles_required('admin')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.marshal_with(message, envelope='response')
    @api.doc("Register a user, without control")
    @api.expect(user_parser, validate=True)
    def post(self):
        args = user_parser.parse_args()
        return self._post(args=args, restricted=False)


@api.route('/send_confirm_mail')
class AuthRegisterSendConfirmMailAPI(CommonRegisterAPI):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Envoie du mail de confirmation")
    @api.expect(user_parser_email, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C0100', 'text': None}
        args = user_parser_email.parse_args()
        user = UserDB.query.filter_by(email=args['email']).first()
        if user is None:
            return_message['code_app'] = 'C0108'
            return_message['text'] = 'Il n\'existe pas de compte avec cet email.'
            return return_message, 404
        if user.confirmed is True:
            return_message['code_app'] = 'C0110'
            return_message['text'] = 'Le compte est déjà vérifié.'
            return return_message, 401
        self._send_mail(args['email'])
        return_message['success'] = True
        return_message['code_app'] = 'C0109'
        return_message['text'] = 'Le mail a été envoyé à cet email.'
        return return_message, 200


@api.route('/confirm/<string:token>')
class ConfirmRegisterAPI(Resource):
    @api.response(201, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Confirmation de l'utilisateur")
    def get(self, token):
        return_message = {'success': False, 'code_app': 'C0100', 'text': None}
        try:
            email = confirm_token(token)
        except:
            return_message['code_app'] = 'C0105'
            return_message['text'] = 'Le lien est invalide ou a expiré.'
            return return_message, 401
        user = UserDB.query.filter_by(email=email).first()
        if user is None:
            return_message['code_app'] = 'C0105'
            return_message['text'] = 'Le lien est invalide ou a expiré.'
            return return_message, 401
        if user.confirmed is True:
            return_message['code_app'] = 'C0106'
            return_message['text'] = 'Compte déjà confirmé. Merci de vous connecter.'
            return return_message, 200
        user.set_confirmed(user.id)
        return_message['success'] = True
        return_message['code_app'] = 'C0107'
        return_message['text'] = 'Compte confirmé.'
        return return_message, 201


@api.route('/login')
class AuthLoginAPI(Resource):
    @api.response(200, 'OK')
    @api.marshal_with(login_message, envelope='response')
    @api.doc("Login a user")
    @api.expect(user_parser, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C0200', 'text': None, 'data': {'auth_token': None}}
        args = user_parser.parse_args()
        user_db = UserDB.query.filter_by(email=args['email']).first()
        if user_db is None:
            user_db = UserDB.query.filter_by(email=args['email'].lower()).first()
        if user_db is None:
            return_message['code_app'] = 'C0202'
            return_message['text'] = 'L\'email "' + args['email'] + '" n\'existe pas'
            return return_message, 404
        if user_db.check_password(args['password']) is False:
            time.sleep(2)
            return_message['code_app'] = 'C0203'
            return_message['text'] = 'Le mot de passe donné est incorrect'
            return return_message, 401
        role = RoleDB().query.filter_by(code_text="admin").first()
        user_role = UserByRoleDB().query.filter_by(user_id=user_db.id, role_id=role.id).first()
        if user_role is None:
            if user_db.confirmed is False or user_db.confirmed is None:
                return_message['code_app'] = 'C0204'
                return_message['text'] = 'Le compte n\'est pas activé. ' \
                                         'Merci de confirmer votre compte avec le lien présent dans le mail envoyé ' \
                                         'lors de l\'enregistrement'
                return return_message, 401
        return_message['success'] = True
        return_message['code_app'] = 'C0201'
        return_message['text'] = 'Connexion réussie'
        return_message['data']['auth_token'] = encode_auth_token(user_db.id)
        return_message['data']['user_id'] = user_db.id
        return_message['data']['user_username'] = user_db.get_username()
        return return_message, 200


@api.route('/logout')
class AuthLogoutAPI(Resource):
    @api.response(200, 'OK')
    @api.marshal_with(message, envelope='response')
    @api.doc("Logout a user")
    @api.expect(user_parser_email, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C0200', 'text': None}
        args = user_parser_email.parse_args()
        user_db = UserDB.query.filter_by(email=args['email']).first()
        if user_db is None:
            return_message['code_app'] = 'C0302'
            return_message['text'] = 'L\'email "' + args['email'] + '" n\'existe pas'
            return return_message, 404
        # TODO Blacklist the TOKEN
        return_message['success'] = True
        return_message['code_app'] = 'C0301'
        return_message['text'] = 'Logout OK'
        return return_message, 200


@api.route('/change_password')
class AuthChangePasswordAPI(Resource):
    @api.response(200, 'OK')
    @auth_token_needed
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Change password")
    @api.expect(user_parser_password, validate=True)
    def put(self):
        return_message = {'success': False, 'code_app': 'C0400', 'text': None}
        args = user_parser_password.parse_args()
        user_id = get_id_from_token()[0]
        user_db = UserDB.query.filter_by(id=user_id).first()
        user_db.set_password(args['password'])
        return_message['success'] = True
        return_message['code_app'] = 'C0401'
        return_message['text'] = 'Changement de mot de passe OK'
        return return_message, 200


@api.route('/users')
class ListUsersAPI(Resource):
    @roles_required('admin')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.marshal_list_with(user, envelope='response')
    def get(self):
        users_list = UserDB.query.all()
        return users_list, 200


@api.route('/users/<string:email>')
@api.route('/users/<int:user_id>')
class UsersAPI(Resource):
    @roles_required('admin')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.marshal_with(user, envelope='response')
    def get(self, email=None, user_id=None):
        user_db = None
        if email is not None:
            user_db = UserDB.query.filter_by(email=email).first()
        if user_id is not None:
            user_db = UserDB.query.filter_by(id=user_id).first()
        if user_db is not None:
            return user_db, 200
        return None, 404

    @roles_required('admin')
    @auth_token_needed
    @api.doc(security='apikey')
    @api.marshal_with(message, envelope='response')
    def delete(self, email=None, user_id=None):
        return_message = {'success': False, 'code_app': 'C0500', 'text': None}
        user_db = None
        if email is not None:
            user_db = UserDB.query.filter_by(email=email).first()
        if user_id is not None:
            user_db = UserDB.query.filter_by(id=user_id).first()
        if user_db is not None:
            email = user_db.email
            user_db.delete()
            return_message['success'] = True
            return_message['code_app'] = 'C0601'
            return_message['text'] = 'L\'utilisateur ' + email + ' a été supprimé'
            return return_message, 200
        return_message['code_app'] = 'C0602'
        return_message['text'] = 'L\'utilisateur ' + email + ' n\'existe pas'
        return return_message, 404


@api.route('/change_role')
class AuthAddGroupAPI(Resource):
    @api.response(200, 'OK')
    @roles_required('admin')
    @auth_token_needed
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a role to a user")
    @api.expect(user_parser_role, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C0500', 'text': None}
        args = user_parser_role.parse_args()
        user_db = UserDB().query.filter_by(email=args['email']).first()
        if user_db is not None:
            role = RoleDB().query.filter_by(code_text=args['role']).first()
            if role is not None:
                user_role = UserByRoleDB().query.filter_by(user_id=user_db.id, role_id=role.id).first()
                if user_role is None:
                    UserByRoleDB().add(user_db.id, role.id)
                    return_message['success'] = True
                    return_message['code_app'] = 'C0501'
                    return_message['text'] = 'Ajout du rôle ' + args['role'] + ' OK'
                    return return_message, 201
                return_message['code_app'] = 'C0504'
                return_message['text'] = 'Rôle ' + args['role'] + ' déjà affecté à l\'utilisateur.'
                return return_message, 200
            return_message['code_app'] = 'C0502'
            return_message['text'] = 'Role ' + args['role'] + ' n\'existe pas.'
            return return_message, 404
        return_message['code_app'] = 'C0503'
        return_message['text'] = 'L\'utilisateur avec l\'email ' + args['email'] + ' n\'existe pas.'
        return return_message, 404

    @api.response(204, 'OK')
    @roles_required('admin')
    @auth_token_needed
    @api.marshal_with(message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Remove a role from a user")
    @api.expect(user_parser_role, validate=True)
    def delete(self):
        return_message = {'success': False, 'code_app': 'C0600', 'text': None}
        args = user_parser_role.parse_args()
        user_db = UserDB().query.filter_by(email=args['email']).first()
        if user_db is not None:
            role = RoleDB().query.filter_by(code_text=args['role']).first()
            if role is not None:
                user_role = UserByRoleDB().query.filter_by(user_id=user_db.id, role_id=role.id).first()
                if user_role is not None:
                    user_role.remove()
                    return_message['success'] = True
                    return_message['code_app'] = 'C0601'
                    return_message['text'] = 'Rôle enlevé OK'
                    return return_message, 204
                return_message['code_app'] = 'C0604'
                return_message['text'] = 'L\'utilisateur n\'a pas le rôle spécifié'
                return return_message, 201
            return_message['code_app'] = 'C0602'
            return_message['text'] = 'Rôle ' + args['role'] + '\'existe pas'
            return return_message, 404
        return_message['code_app'] = 'C0603'
        return_message['text'] = 'L\'utilisateur avec l\'email ' + args['email'] + ' n\'existe pas'
        return return_message, 404


@api.route('/check_role/<string:role>')
class UserCheckRole(Resource):
    @auth_token_needed
    @api.doc(security='apikey')
    def get(self, role):
        user_id = get_id_from_token()[0]
        role = RoleDB().query.filter_by(code_text=role).first()
        if role is None:
            return False, 404
        user_role = UserByRoleDB().query.filter_by(user_id=user_id, role_id=role.id).first()
        if user_role is not None:
            return True, 200
        return False, 200
