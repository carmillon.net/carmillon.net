#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask_restplus import Resource, Namespace
from carmillon.api.common import status_parser, status_list_message,auth_token_needed, brand_parser, roles_required,\
    get_id_from_token
from carmillon.models.status import StatusDB

api = Namespace('Status', description='Operations sur les status')


@api.route('/')
@api.route('')
class DeptAPI(Resource):
    @api.response(200, 'OK')
    @api.response(404, 'Not found')
    @api.doc("Get a status")
    @api.doc(security='apikey')
    @api.marshal_with(status_list_message, envelope='response')
    @auth_token_needed
    @api.expect(status_parser, validate=True)
    def get(self):
        return_message = {'success': False, 'code_app': 'C2000', 'text': None, 'data': []}
        args = status_parser.parse_args()
        if args['id'] is not None:
            result = StatusDB().query.filter_by(id=args['id']).all()
        elif args['code'] is not None:
            result = StatusDB().query.filter_by(code_text=args['code']).all()
        elif args['nom'] is not None:
            result = StatusDB().query.filter_by(name_text=args['nom']).all()
        else:
            result = StatusDB().query.order_by("name_text").all()
        for i in result:
            return_message['data'].append(i)
        return_message['success'] = True
        return_message['code_app'] = 'C2001'
        return_message['text'] = 'Search OK'
        return return_message, 200

    @api.response(201, 'Create')
    @api.response(404, 'Not found')
    @api.marshal_with(status_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a status")
    @roles_required('modo')
    @api.expect(status_parser, validate=True)
    def post(self):
        return_message = {'success': False, 'code_app': 'C2100', 'text': None}
        args = status_parser.parse_args()
        if args['code'] is None or 'code' not in args:
            if args['nom'] is None or 'nom' not in args:
                return_message['code_app'] = 'C2101'
                return_message['text'] = 'Error, you need to specify a code or a name'
                return return_message, 401
            StatusDB(name_text=args['nom'], code_text=args['nom']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2102'
            return_message['text'] = 'Adding OK (code missing)'
            return return_message, 201
        if args['nom'] is None or 'nom' not in args:
            StatusDB(name_text=args['code'], code_text=args['code']).add()
            return_message['success'] = True
            return_message['code_app'] = 'C2103'
            return_message['text'] = 'Adding OK (name missing)'
            return return_message, 201
        StatusDB(name_text=args['nom'], code_text=args['code']).add()
        return_message['success'] = True
        return_message['code_app'] = 'C2104'
        return_message['text'] = 'Adding OK'
        return return_message, 201

    @api.response(201, 'Modified')
    @api.response(404, 'Not found')
    @api.marshal_with(status_list_message, envelope='response')
    @api.doc(security='apikey')
    @api.doc("Add a brand")
    @roles_required('modo')
    @api.expect(brand_parser, validate=True)
    def put(self):
        return_message = {'success': False, 'code_app': 'C2200', 'text': None}
        args = brand_parser.parse_args()

        if args['id'] is None or 'id' not in args:
            return_message['code_app'] = 'C2201'
            return_message['text'] = 'You need to specify an id'
            return return_message, 401

        dept = StatusDB().query.filter_by(id=args['id']).first()

        if 'code' in args and args['code'] is not None:
            dept.set_code(args['code'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Code change.'
            else:
                return_message['text'] = return_message['text'] + 'Code change.'

        if 'nom' in args and args['nom'] is not None:
            dept.set_name(args['nom'], get_id_from_token()[0])
            if return_message['text'] is None:
                return_message['text'] = 'Name change.'
            else:
                return_message['text'] = return_message['text'] + 'Name change.'

        if return_message['text'] is None:
            return_message['text'] = 'Nothing to change'

        return_message['success'] = True
        return_message['code_app'] = 'C2201'
        return return_message, 200

    @api.response(200, 'OK')
    @api.response(204, 'OK')
    @auth_token_needed
    @roles_required('admin')
    @api.doc(security='apikey')
    @api.doc("Delete a status")
    @api.expect(status_parser, validate=True)
    @api.marshal_with(status_list_message, envelope='response')
    def delete(self):
        return_message = {'success': False, 'code_app': 'C2300', 'text': None}
        args = status_parser.parse_args()
        if args['id'] is None:
            return_message['text'] = 'Un ID doit être spécifié.'
            return_message['code_app'] = 'C2301'
            return return_message, 401
        status = StatusDB().query.filter_by(id=args['id']).first()
        if status is None:
            return_message['text'] = 'Le statut n\'existe pas.'
            return_message['code_app'] = 'C2602'
            return return_message, 404
        status.delete()
        return_message['success'] = True
        return_message['code_app'] = 'C2603'
        return_message['text'] = 'Suppression effectuée'
        return return_message, 200