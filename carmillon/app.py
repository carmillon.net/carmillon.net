#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

from flask import Flask

from carmillon.config import DevConfig, TestConfig, ProdConfig


def create_app(config=None, config_data=None):
    app = Flask(__name__)

    if os.environ.get('FLASK_ENV') == "production" or config == "production":
        app.config.from_object(ProdConfig)
    elif os.environ.get('FLASK_ENV') == "testing" or config == "testing":
        app.config.from_object(TestConfig)
    elif os.environ.get('FLASK_ENV') == "development" or config == "development":
        app.config.from_object(DevConfig)
    else:
        print("Error, FLASK_ENV is empty, please provide a value (production, testing or development")
        return None

    if config_data is not None:
        for i in config_data:
            app.config[i] = config_data[i]

    from carmillon.extensions import db, migrate
    db.init_app(app)
    migrate.init_app(app, db)

    if app.config['CORS_ENABLE'] is True:
        from carmillon.extensions import cors
        cors.init_app(app)

    from carmillon.api.routes import api
    api.init_app(app)

    from carmillon.extensions import login_manager
    login_manager.init_app(app)

    from carmillon.extensions import mail
    mail.init_app(app)

    # Flask-Login configuration
    from carmillon.models.users import UserDB

    @login_manager.user_loader
    def load_user(user_id):
        return UserDB.get(user_id)

    return app
