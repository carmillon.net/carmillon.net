#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# https://adresse.data.gouv.fr/api
import requests
import time
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


class Geocoding():
    def __init__(self):
        # https://adresse.data.gouv.fr/api
        self.adresse_url = "https://api-adresse.data.gouv.fr/"
        self.geo_url = "https://geo.api.gouv.fr/"

    def adresse(self, text=None):
        search_text = text.replace(" ", "+")
        url = self.adresse_url + "search/?q="
        data = self.request_json(url, search_text)
        if "features" in data:
            if len(data["features"]) >= 1:
                district = None
                if "district" in data["features"][0]["properties"]:
                    district = data["features"][0]["properties"]["district"]
                return {"lon": data["features"][0]["geometry"]["coordinates"][0],
                        "lat": data["features"][0]["geometry"]["coordinates"][1],
                        "label": data["features"][0]["properties"]["label"],
                        "score": data["features"][0]["properties"]["score"],
                        "city": data["features"][0]["properties"]["city"],
                        "district": district,
                        "postcode": data["features"][0]["properties"]["postcode"],
                        "context": data["features"][0]["properties"]["context"].split(", ")}
                # return data["features"][0]
        return None

    def coord(self, lat, lon):
        url = self.adresse_url + "reverse/?lon=" + str(lon) + "&lat=" + str(lat)
        data = self.request_json(url)
        if "features" in data:
            if len(data["features"]) >= 1:
                district = None
                if "district" in data["features"][0]["properties"]:
                    district = data["features"][0]["properties"]["district"]
                return {"lon": data["features"][0]["geometry"]["coordinates"][0],
                        "lat": data["features"][0]["geometry"]["coordinates"][1],
                        "label": data["features"][0]["properties"]["label"],
                        "score": data["features"][0]["properties"]["score"],
                        "city": data["features"][0]["properties"]["city"],
                        "district": district,
                        "postcode": data["features"][0]["properties"]["postcode"],
                        "context": data["features"][0]["properties"]["context"].split(", ")}
                # return data["features"][0]
        return None

    def commune(self, text=None, cp=None):
        if text is not None:
            search_text = text.replace(" ", "+")
            url = self.geo_url + "communes?nom="
            data = self.request_json(url, search_text)
            if len(data) >= 1:
                return {
                    "nom": data[0]["nom"],
                    "code_postal": data[0]["codesPostaux"][0],
                }

    def gare(self, gare):
        url = "https://data.sncf.com/api/records/1.0/search/?dataset=referentiel-gares-voyageurs"
        text = "&q=" + gare
        text = text + "&sort=intitule_gare" \
              "&facet=agence_gare" \
              "&facet=region_sncf" \
              "&facet=unite_gare" \
              "&facet=departement" \
              "&facet=segment_drg"
        data = self.request_json(url=url, text=text)
        code_postal = str(data["records"][0]["fields"]["code_postal"])
        if len(code_postal) == 4:
            code_postal = "0" + code_postal
        lat = data["records"][0]["fields"]["latitude_wgs84"]
        lon = data["records"][0]["fields"]["longitude_wgs84"]
        geo = self.coord(lat=lat, lon=lon)
        return {
            "nom": data["records"][0]["fields"]["intitule_fronton_de_gare"],
            "lat": lat,
            "lon": lon,
            "code_postal": code_postal,
            "city": geo["city"],
            "context": geo["context"],
        }

    def request_json(self, url, text=None):
        if text is None:
            quote_page = url
        else:
            quote_page = url + text
        s = requests.Session()
        t0 = time.time()
        try:
            r = self.requests_retry_session(session=s).get(quote_page, timeout=5)
            response = r.json()
            return response

        except Exception as x:
            t1 = time.time()
            print("Error : %s, time : %s" % (x.__class__.__name__, t1 - t0))
            """
            current_app.logger.info('request price fail (%s) on %s (%s s)',
                                    x.__class__.__name__,
                                    quote_page,
                                    t1 - t0
                                    )
            """
            return None

    def requests_retry_session(self,
                               retries=3,
                               backoff_factor=0.3,
                               status_forcelist=(500, 502, 504),
                               session=None,
                               ):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session


if __name__ == '__main__':
    print("Test adresse: %s", Geocoding().adresse("16 Avenue Thiers 06300 NICE"))
#    print("Test commune: %s", Geocoding().commune(text="PUTEAUX"))
#    print("Test gare: %s", Geocoding().gare("BERCY GARE"))
