#!/bin/bash
# https://www.jonathan-petitcolas.com/2015/09/28/dump-docker-ized-database-to-amazon-s3.html

# Configuration
CONTAINER_NAME="$(docker ps -aqf "name=carmillon_db*")"
FILENAME="db-`date +%Y-%m-%d-%H:%M:%S`.sql"
DUMPS_FOLDER="/path/to/backup/directory"
BUCKET_NAME="carmillon-backup"
BZIP_EXTENSION=".bz2"

# Backup from docker container
docker exec $CONTAINER_NAME sh -c "PGPASSWORD=\"\$POSTGRES_PASSWORD\" pg_dump -U postgres postgres > /tmp/$FILENAME"
docker cp $CONTAINER_NAME:/tmp/$FILENAME $DUMPS_FOLDER
docker exec $CONTAINER_NAME sh -c "rm /tmp/db-*.sql"

cd $DUMPS_FOLDER && bzip2 $FILENAME

# Upload on S3 (Wasabi)
/usr/bin/aws s3 cp $DUMPS_FOLDER$FILENAME$BZIP_EXTENSION s3://$BUCKET_NAME --endpoint-url=https://s3.eu-central-1.wasabisys.com