#!/bin/bash
#
#    Copyright (C) 2019  Julien LAVERGNE
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
set -e

cd /opt
export PYTHONPATH="${PYTHONPATH}:/opt"
export FLASK_ENV=production
export FLASK_APP=carmillon
flask db upgrade
cd /
exec gunicorn --forwarded-allow-ips="*" -c python:config.gunicorn 'carmillon:app.create_app()'
